<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-cogs"></i> Filter</h3>
			</div>
			<div class="panel-body">
				<form class="form-inline" role="form" method="POST" action="">
					<div class="form-group">
					
						<input type="text" name="st_name_id" class="form-control" value="<?php echo isset($_POST['st_name_id'])?$_POST['st_name_id']:""; ?>" placeholder="name or code" />
					</div>
					<div class="form-group">
		                  <select class="form-control" id="st_stage" name="st_stage">
		                     <option value="0">none</option>
							<?php 
								for ($i=1; $i <= 5; $i++) { 
									$selected = isset($_POST['st_stage']) && $_POST['st_stage']==$i?"selected":"";
		                     		echo "<option {$selected} value='{$i}'>{$i}</option>";
									
								}
							?>
		                     
		                  </select>
		            </div>
		            <div class="form-group">
		                  <select class="form-control" id="st_dep" name="st_dep">
							<?php 
								$selected1 = isset($_POST['st_dep']) && $_POST['st_dep']== "Computer Science" ?"selected":"";
								$selected2 = isset($_POST['st_dep']) && $_POST['st_dep']== "Gas & Oil" ?"selected":"";
							?>
		                     <option value="0">none</option>
		                     <option <?php echo $selected1; ?> value="Computer Science">Computer Science</option>
		                     <option <?php echo $selected2; ?> value="Gas & Oil">Gas & Oil</option>
		                  </select>
		            </div>
					<div class="form-group">
						<button type="submit" name="search" class="btn btn-default">search</button>
					</div>
						
				</form>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="student-table col-md-10">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-users'></span> Deactivated Students List
			</div>
		</div>
		<div class="panel-body flip-scroll">
			<table class="student-table table-bordered table-striped table-condensed flip-content">
			<caption style="text-align:left;"><a style="background-color:#DFF0D8;color:#3C763D;padding:0px 5px 0px 5px" 
					href="./index.php">click here to go back </a></caption>
			<thead class="flip-content">
			<tr>
				<th>
					 #
				</th>
				<th>
					code
				</th>
				<th>
					name
				</th>
				<th>
					 stage
				</th>
				<th>
					 department
				</th>
				<th>
					 stage
				</th>
				<th class="numeric">
					 phone number
				</th>
				
				<th >
					 tools
				</th>
			</tr>
			</thead>
			<tbody>
<?php
	$from=0;
	$count=15;
	$side_counter = 1;
	if (isset($_GET['from']) && is_numeric($_GET['from'])) 
	{
		$from = $_GET['from'];
		$side_counter = $from+1;
		//$count
	}
	$output = "";
	$search = 0;
	if (isset($_POST['search']) && ((isset($_POST['st_name_id']) && !empty($_POST['st_name_id'])) || (isset($_POST['st_dep']) && !empty($_POST['st_dep']) && $_POST['st_dep']!='none') || (isset($_POST['st_stage']) && !empty($_POST['st_stage']) && $_POST['st_stage']!='none'))) {
		$count = 99999; // this is temporary
		$students_set = get_students($from, $count, 0, $_POST['st_name_id'], $_POST['st_dep'], $_POST['st_stage'], 1);
		$students_set_modal = get_students($from, $count, 0, $_POST['st_name_id'], $_POST['st_dep'], $_POST['st_stage'], 1);
		$search = 1;
	}else{
		$students_set = get_students($from, $count, 0, 0,0,0, 1);
		$students_set_modal = get_students($from, $count, 0, 0,0,0, 1);
		$search = 0;
	}
	while ($student = mysql_fetch_assoc($students_set)) {
		$output .= "<tr id='{$student['st_id']}'>
						<td>
							{$side_counter}
						</td>
						<td>
							{$student['st_id']}
						</td>
						<td>
							 {$student['fname']} {$student['mname']} {$student['lname']}
						</td>
						<td class='numeric'>
							 {$student['stage']}
						</td>
						<td>
							{$student['dep']}
						</td>
						<td >
							 {$student['address']}
						</td>
						<td class='numeric'>
							 {$student['phone_no']}
						</td>
						<td>
							 <a href='javascript:;' role='button' class='btn default btn-xs red activate_student'>
								<i class='fa fa-check-square-o'></i> activate </a>
							<a class='btn default btn-xs blue' data-backdrop='static'  data-toggle='modal' href='#basic{$student['st_id']}'>
								<i class='fa fa-share'></i> view detailed info </a>
						</td>
					</tr>";
					$side_counter++;
	}
	echo $output;
?>
		</tbody>
	</table>
	<div>
		<ul class="pagination">
			<li>
				<?php
					$student_count = student_counter(1);
					$student_count = mysql_fetch_assoc($student_count);
					$student_count = ceil($student_count['counter']/$count);

					$prev=$from>0?$from-$count:0;
					$next=($from+$count)<$student_count*$count?$from+$count:$from;
				?>
				<a href="index.php?page=homepage&from=<?php echo $prev; ?>">
				<i class="fa fa-angle-left"></i>
				</a>
			</li>
			<?php
				$output="";
				for ($c=0; $c <$student_count ; $c++) 
				{
					$class = ceil($from/$count)==$c?'active':'';
					// echo ceil($from/$count);
					// echo $from;

					$output .= "<li class='{$class}'><a href='index.php?page=homepage&from=";
					$output .= $c*$count;
					$output .= "'>";
					$output .= $c+1;
					$output .= " </a></li>";
				}
				echo $output;
			?>
			<li>
				<a href="index.php?page=homepage&from=<?php echo $next; ?>">
				<i class="fa fa-angle-right"></i>
				</a>
			</li>
		</ul>
	</div>
	</div>
	</div>
	</div>
	<div class="col-md-2">

	</div>
	<div class="row">
		<div class="col-md-12">
		</div>
	</div>
</div>
<?php 
$modal="";
while ($student = mysql_fetch_assoc($students_set_modal)) {
	// $student_fee_info_set = student_fee_report($student['st_id']);
	// $total_fee = $student['fee'] - $student['discount'];
	// $payed_fee = payed_fee($student['st_id']);
	$owed_fees="<hr />";
	$student_owed_fee_set = student_owed_fee($student['st_id'], -1);
	while ($student_owed_fee = mysql_fetch_assoc($student_owed_fee_set)) {
		$get_st_payed_fee = get_st_payed_fee($student_owed_fee['owed_id']);
		//$ss = var_dump($get_st_payed_fee);
		$payment_buttons="";
		if ($student_owed_fee['payment_completed']<1 && $student['view']==1) {
			$payment_buttons ="
			<a title='pay fee' href='index.php?page=pay_fee&st_id={$student['st_id']}&owed_fee={$student_owed_fee['owed_id']}&total_fee={$student_owed_fee['total_fee']}' class='btn btn-xs btn-primary'><i class='fa fa-plus'></i></a>
			<a title='edit fee' href='index.php?page=edit_fee&owed_fee={$student_owed_fee['owed_id']}&st_name={$student['fname']} {$student['mname']} {$student['lname']}' class='btn btn-xs purple'><i class='fa fa-edit'></i></a>
			";
		}
		$owed_fees .= 
			"
			<p class='fee-modal'>
			<b>fee title: </b> {$student_owed_fee['note']} <br />
			<b>year: </b> {$student_owed_fee['year']} <br />
			<b>fee: </b> {$student_owed_fee['fee']} <br />
			<b>discount: </b> {$student_owed_fee['discount']} <br />
			<b>total fee: </b> {$student_owed_fee['total_fee']} <br />
			<b>payed fee: </b> {$get_st_payed_fee} <br />
			{$payment_buttons}
			<a title='view report' href='index.php?page=reports&st_id={$student['st_id']}&owed_id={$student_owed_fee['owed_id']}&deleted=1' class='btn btn-xs red'><i class='glyphicon glyphicon-eye-open'></i></a>
			</p>
			<br />
			";
	}

 	$modal .= "
				<div class='modal fade' id='basic{$student['st_id']}' tabindex='-1' role='basic' aria-hidden='true'>
				<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-hidden='true'></button>
						<h4 class='modal-title'>Student Information</h4>
					</div>
					<div class='modal-body modal-body-scroll'>
					
					
					
							<b>Code: </b>{$student['st_id']}<br />
							<b>Full Name: </b>{$student['fname']} {$student['mname']} {$student['lname']}<br />
							<b>D.O.B:</b>{$student['dob']}<br />
							<b>Address: </b>{$student['address']}<br />
							<b>Phone Number: </b> {$student['phone_no']} <br />
							<b>Department: </b>{$student['dep']}<br />
							<b>Stage: </b>{$student['stage']}<br />
							{$owed_fees}
					
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn default red' data-dismiss='modal'><i class='fa fa-times-square-o'></i> close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		";
	}
	echo $modal;
 ?>
