<?php  
if ($_SESSION['admin']==1 || $_SESSION['admin']==3)
{
   $st_id=0;
   $owed_id=0;
   $total_fee=0;
   if (
      isset($_GET['st_id']) && !empty($_GET['st_id']) 
      && isset($_GET['owed_fee']) && !empty($_GET['owed_fee']) 
      && isset($_GET['total_fee']) && !empty($_GET['total_fee'])) {

      $st_id = $_GET['st_id'];
      $owed_id = $_GET['owed_fee'];
      $total_fee = $_GET['total_fee'];

   }
?>
<div class="contact row">
	<div class="add-panel col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Pay Student Fee</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" name="payform" onsubmit="return validateForm()" action="index.php?page=print_invoice" method="POST" >
            <div class="form-group">
               <label for="name_code" class="col-md-3 control-label">Name and Code of Student</label>
               <div class="col-md-7">
                  <select required="required" class="select2 input-default" id="name_code" name="code">
                  <?php 
                     $student_names_set = get_students();
                     while ($student_names = mysql_fetch_assoc($student_names_set)) {
                        $selected = $student_names['st_id'] == $st_id?"selected":"";
                        echo "<option {$selected} value='{$student_names['st_id']}'>{$student_names['st_id']}, {$student_names['fname']} {$student_names['mname']} {$student_names['lname']}</option>";
                     }
                     $first_st_id = isset($st_id) && !empty($st_id)?$st_id:mysql_result($student_names_set, 0, 'st_id');
                  ?>
                  </select>
               </div>
            </div>

            <div class="form-group">
               <label for="fee_type" class="col-md-3 control-label">Fee Type</label>
               <div class="col-md-7">
                  <select required="required" class="input-default" id="fee_type" name="fee_type">
                  <option value="0">none</option>
                  <?php 
                     $st_owed_fee_list_set = get_st_owed_fee_list($first_st_id);
                     while ($st_owed_fee_list = mysql_fetch_assoc($st_owed_fee_list_set)) {
                        $selected = $st_owed_fee_list['owed_id'] == $owed_id?"selected":"";
                        echo "<option {$selected} value='{$st_owed_fee_list['owed_id']}'>{$st_owed_fee_list['fee_title']} - {$st_owed_fee_list['year']}</option>";
                     }
                     $first_owed_id = isset($owed_id) && !empty($owed_id)?$owed_id:mysql_result($st_owed_fee_list_set, 0, 'owed_id');
                     $first_owed_total_fee = isset($total_fee) && !empty($total_fee)?$total_fee:mysql_result($st_owed_fee_list_set, 0, 'total_fee');
                     //st_owed_fee_list['total_fee']
                  ?>
                  </select>
               </div>
            </div>
            <div class="form-group" id="loading" style="display:none">
               <label for="" class="col-md-3 control-label">please wait</label>
               <div class="col-md-7">
                  <img src="./images/loading.gif">
               </div>
            </div>
            <div class="form-group loaded">
               <label for="owed_fee" class="col-md-3 control-label">owed fee</label>
               <div class="col-md-7">
                  <input required="required" readonly="readonly" type="number" min="0" value="<?php echo $first_owed_total_fee; ?>" class="form-control" id="owed_fee" name="owed_fee">
               </div>
            </div>

            <div class="form-group loaded">
               <label for="payed_fee" class="col-md-3 control-label">payed fee</label>
               <div class="col-md-7">
                  <?php 
                  $first_owed_id = !$first_owed_id?0:$first_owed_id;
                  $init_payed_fee = get_st_payed_fee($first_owed_id);
                   ?>
                  <input required="required" readonly="readonly" type="number" min="0" value="<?php echo $init_payed_fee; ?>" class="form-control" id="payed_fee" name="payed_fee">
               </div>
            </div>
            <div class="form-group loaded">
               <label for="remaining_fee" class="col-md-3 control-label">remaining fee</label>
               <div class="col-md-7">
                  <input required="required" readonly="readonly" type="number" min="0" value="<?php echo $first_owed_total_fee - $init_payed_fee; ?>" class="form-control" id="remaining_fee" name="remaining_fee">
               </div>
            </div>

            <div class="form-group">
               <label for="amount" class="col-md-3 control-label">amount</label>
               <div class="col-md-7">
                  <input required="required" type="number" min="0" type="text" class="form-control" id="amount" name="amount">
               </div>
            </div>
            <div class="form-group">
               <label for="datetimepicker1" class="col-md-3 control-label">date</label>
               <div class="col-md-7">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input required="required" type='text' class="form-control" name="qist_date" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
			   <div class="form-group">
               <label for="mob_no" class="col-md-3 control-label">note</label>
               <div class="col-md-7">
                  <textarea required="required" class="form-control" id="note" name="note"></textarea>
               </div>
            </div>

            <div class="form-group">
               <div class= "col-md-9">
                  <button type="submit" name="pay_fee" class="btn btn-info actionbutton">ok</button>
               </div>
            </div>
         </form>
			</div>
		</div>
	</div>
</div>
<script>
      $('.select2').select2({ placeholder : '' });

      $('.select2-remote').select2({ data: [{id:'A', text:'A'}]});

      $('button[data-select2-open]').click(function(){
        $('#' + $(this).data('select2-open')).select2('open');
      });

function validateForm() {
    var x = document.forms["payform"]["fee_type"].value;
    if (x == null || x == "" || x == '0'|| x == 0) {
        alert("you can't continue");
        return false;
    }
}
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {
        $("#amount").on('change keyup paste', function(e){

         if (parseInt($("#amount").val(),10) > parseInt($("#remaining_fee").val(),10)) {
            // alert("discount should not be more than owed fee!");
            $("#amount").val(0);
         };
      });
    });
</script>


<?php  
} //admin role IF
?>