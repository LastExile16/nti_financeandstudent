<?php  
if ($_SESSION['admin']==1 || $_SESSION['admin']==2 || $_SESSION['admin']==3)
{
?>

<?php
	//if request came from spend_expense.php
	if (isset($_POST['spend_expense'])) {
		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";
    
		$exp_date = $_POST['exp_date'];
		$amount = $_POST['amount'];
		$note = safe(trim($_POST['note']));
		$accountant = $_SESSION['fullname'];
		$invoice_code = $_POST['inv_number'];
		$exp_type = $_POST['exp_type'];	
		$exp_type_name = get_expense_type_name($exp_type);
    	$query = "INSERT INTO expenses(e_id_f, amount, date, note, user_id_f, invoice_number) VALUES
					({$exp_type}, {$_POST['amount']}, '{$_POST['exp_date']}', '{$note}', 
						{$_SESSION['user_id']}, '{$_POST['inv_number']}')";
		mysql_query($query) or die("Q1.printingInvoiceExpense".mysql_error());
		

	}
	else{

		//request came from expenses.php

		$ex_id = 0;
		if (isset($_GET['ex_id'])) {
			$ex_id = $_GET['ex_id'];
		}
		$exp_type_name = get_expense_type_name($ex_id);

		$expense_report_info_set = get_expense_report_info(0, $ex_id);
		$expense_report_info = mysql_fetch_assoc($expense_report_info_set);
		
		$exp_date = $expense_report_info['date'];
		$amount = $expense_report_info['amount'];
		$note = $expense_report_info['note']; 
		$accountant = get_invoice_accountant($expense_report_info['user_id_f']);
		$invoice_code = $expense_report_info['invoice_number'];

	}
?>
<style type="text/css">
	@media print{
		.top-header, .copywrite, .top-menu, .print-button{
			display: none;
		}
		.panel-body{
			margin-top: 2cm;
		}
	}
	.to_right{
		float: right;
	}
	.pad_it{
		padding-bottom: 15em !important;
		padding-top: 2em !important;
	}
	table{
		table-layout:fixed;
		word-wrap:break-word;
	}
</style>
<div class="row">
	<div class="col-md-9 col-sm-12 col-xs-12 student-table">
		<div class="panel-body flip-scroll">
		<caption><span class="invoice_code">invoice no.: <?php echo $invoice_code; ?></span><span class="to_right">date: <?php echo $exp_date; ?></span></caption>
			<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class="flip-content">
			<tr>
				<th>
					 expense type
				</th>
				
				<th>
					 amount
				</th>
				<th>
					 note
				</th>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$output = "<tr>
							<td class='pad_it'>
								Institute Expenses - {$exp_type_name}
							</td>
							<td class='pad_it'>
								{$amount}
							</td>
							<td class='pad_it'>
								 {$note}
							</td>
						</tr>";			
			echo $output;
		?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3">accountant name: <br/> <?php echo $accountant; ?></td>
				</tr>
			</tfoot>
			</table>
		</div>
	</div>
	
</div>
<div class="row print-button">
   <div class="col-md-9 col-sm-12 col-xs-12" style="float: right; margin-right: 15px;">
      <a href="javascript:window.print();" class="btn btn-defualt btn-lg red"><i class="glyphicon glyphicon-print"></i></a>
   </div>
</div>
<?php 
} // admin role IF
?>