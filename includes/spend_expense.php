<?php  
if ($_SESSION['admin']==1 || $_SESSION['admin']==2 || $_SESSION['admin']==3)
{
   if (isset($_POST['add_usr'])) {

      $full_name = safe(trim($_POST['full_name']));
      $code = safe(trim($_POST['code']));
      $role = $_POST['role'];
      $query = "INSERT INTO teacher_employee(code, full_name, teach_emp, user_id_f)
               VALUES('{$code}', '{$full_name}', {$role}, {$_SESSION['user_id']})";
      mysql_query($query) or die(" Error adding new user . " . mysql_error());
   }
?>
<div class="contact row">
	<div class="add-panel col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Spend a Expense</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="index.php?page=print_invoice_expense" method="POST" >
            <div class="form-group">
               <label for="exp_type" class="col-md-3 control-label">expense type</label>
               <div class="col-md-7">
                  <select class="select2 input-default" id="exp_type" name="exp_type">
                  <?php 
                     $expense_name_set = get_expense_names();
                     while ($expense_name = mysql_fetch_assoc($expense_name_set)) {
                        echo "<option value='{$expense_name['id']}'>{$expense_name['name']}</option>";
                     }
                  ?>
                  </select>
               </div>
            </div>            

            <div class="form-group">
               <label for="inv_number" class="col-md-3 control-label">invoice number</label>
               <div class="col-md-7">
                  <input type="text" required="required" class="form-control" id="inv_number" name="inv_number">
               </div>
            </div>
            
            <div class="form-group">
               <label for="datetimepicker1" class="col-md-3 control-label">date</label>
               <div class="col-md-7">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input type='text' required="required" class="form-control" name="exp_date" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
            
            <!-- <div class="form-group">
               <label for="total_money" class="col-md-3 control-label">total available money</label>
               <div class="col-md-7">
                  <input type="text" readonly="readonly" class="form-control" value="<?php echo "" //get_total_money(); ?>" id="total_money" name="total_money">
               </div>
            </div> -->
            <div class="form-group">
               <label for="amount" class="col-md-3 control-label">amount</label>
               <div class="col-md-7">
                  <input type="number" required="required" class="form-control" id="amount" name="amount">
               </div>
            </div>

			   <div class="form-group">
               <label for="mob_no" class="col-md-3 control-label">note</label>
               <div class="col-md-7">
                  <textarea required="required" class="form-control" id="note" name="note"></textarea>
               </div>
            </div>

            <div class="form-group">
               <div class= "col-md-9">
                  <button type="submit" name="spend_expense" class="btn btn-info actionbutton">ok</button>
               </div>
            </div>
         </form>
			</div>
		</div>
	</div>


<!-- second column -->
<div class="add-panel col-md-6">
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user-plus"></i> Spend a Expense by Employee or Teacher</h3>
         </div>
         <div class="panel-body">
            <form class="form-horizontal" role="form" action="index.php?page=print_invoice_specific_expense" method="POST" >
            <div class="form-group">
               <label for="empteach_id" class="col-md-3 control-label">Teacher or Employee</label>
               <div class="col-md-7">
                  <select class="select2 input-default" required="required" id="empteach_id" name="empteach_id">
                  <?php 
                     $get_empteach_set = get_empteach_set();
                     // id, code, full_name, teach_emp, user_id_f
                     while ($get_empteach = mysql_fetch_assoc($get_empteach_set)) {
                        echo "<option data-set={$get_empteach['teach_emp']} value='{$get_empteach['id']}'>
                        {$get_empteach['code']}, {$get_empteach['full_name']}
                        </option>";
                     }
                  ?>
                  </select>
                  <br />
               <a class="btn default btn-md red" data-backdrop='static' data-toggle='modal' href='#addperson'>
                  <i class="fa fa-plus-square"> add new person</i>
               </a>
               </div>
            </div>
         

            <!-- <div class="form-group">
               <label for="inv_number" class="col-md-3 control-label">invoice number</label>
               <div class="col-md-7">
                  <input type="text" required="required" class="form-control" id="inv_number" name="inv_number">
               </div>
            </div> -->
            
            <div class="form-group">
               <label for="datetimepicker1" class="col-md-3 control-label">date</label>
               <div class="col-md-7">
                  <div class='input-group date datepick' id='datetimepicker1'>
                  <input type='text' required="required" class="form-control" name="exp_date" />
                  <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                     </div>
               </div>
            </div>
            <div class="form-group">
               <label for="bsc_slry_txt" class="col-md-3 control-label">basic salary text</label>
               <div class="col-md-7">
                  <input type="text" required="required" class="form-control" id="bsc_slry_txt" name="bsc_slry_txt">
               </div>
            </div>
            <div class="form-group">
               <label for="amount" class="col-md-3 control-label">amount</label>
               <div class="col-md-7">
                  <input type="number" required="required" class="form-control" id="amount" name="amount">
               </div>
            </div>

            <div class="form-group">
               <label for="others_text" class="col-md-3 control-label">others text</label>
               <div class="col-md-7">
                  <input type="text" class="form-control" id="others_text" name="others_text">
               </div>
            </div>
            <div class="form-group">
               <label for="others_amount" class="col-md-3 control-label">others amount</label>
               <div class="col-md-7">
                  <input type="number" value="0" required="required" class="form-control" id="others_amount" name="others_amount">
               </div>
            </div>

            <div class="form-group">
               <label for="mob_no" class="col-md-3 control-label">note</label>
               <div class="col-md-7">
                  <textarea required="required" class="form-control" id="note" name="note"></textarea>
               </div>
            </div>

            

            <div class="form-group">
               <div class= "col-md-9">
                  <button type="submit" name="spend_specific_expense" class="btn btn-success actionbutton">ok</button>
               </div>
            </div>
         </form>
         </div>
      </div>
   </div>
</div>
<div class='modal fade' id='addperson' tabindex='-1' role='basic' aria-hidden='true'>
   <div class='modal-dialog'>
   <div class='modal-content'>
      <div class='modal-header'>
         <h4 class='modal-title'>Teacher or Employee Information</h4>
      </div>
      <div class='modal-body modal-body-scroll'>
      
         <form class='form-horizontal' action='' method='POST'>
            <div class='form-group'>
               <label for='code' class='col-md-3 control-label'>code:</label>
               <div class='col-md-9'>
                  <input name='code' required='required' type='text' class='form-control'>
               </div>
            </div>
            <div class='form-group'>
               <label for='full_name' class='col-md-3 control-label'>full name:</label>
               <div class='col-md-9'>
                  <input name='full_name' required='required' type='text' class='form-control'>
               </div>
            </div>


            <div class='form-group'>
               <label for='role' required='required' class='col-md-3 control-label'>role:</label>
               <div class='col-md-9'>
                  <select type='text' class='form-control input-sm' name='role'>
                  <option value='1'>teacher</option>
                  <option value='2'>employee</option>
               </select>
               </div>
            </div>
      
      </div>
      <div class='modal-footer'>
         <div class='form-group col-md-4'>
               <button class='form-control btn default' data-dismiss='modal' aria-hidden='true'>cancel</button>
          </div>
          <div class='form-group col-md-4'>
            <button type='submit' name='add_usr' class='form-control btn blue'>Add</button>
          </div>
      </div>
         </form>
   </div>
   <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div> 
<script>
      $('.select2').select2({ placeholder : '' });

      $('.select2-remote').select2({ data: [{id:'A', text:'A'}]});

      $('button[data-select2-open]').click(function(){
        $('#' + $(this).data('select2-open')).select2('open');
      });
    </script>
<script type="text/javascript">
    // When the document is ready
    /*$(document).ready(function () {
        $("#amount").on('change keyup paste', function(e){

         if (parseInt($("#amount").val(),10) > parseInt($("#total_money").val(),10)) {
            $("#amount").val(0);
         };
      });
    });*/
</script>


<?php  
} // admin role IF
?>