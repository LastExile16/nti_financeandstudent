<?php 
if(!isset($_SESSION))
{
	session_start();	
}
require_once('./connection.php');
require_once('./functions.php');
if(logged_in())
{
	if(isset($_POST['code']))
	{
		$fee_type_set = get_st_owed_fee_list($_POST['code']);
		echo "<option value='0'>none</option>";
		if(mysql_num_rows($fee_type_set) > 0)
		{
			while ($fee_type = mysql_fetch_assoc($fee_type_set))
  			{
  				echo "<option value='{$fee_type['owed_id']}'>{$fee_type['fee_title']}</option>";
  			}
		}
	}
}
else
{
	header('Location:../index.php');
	exit;
}
?>