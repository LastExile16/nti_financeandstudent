<div class="contact row">
	<div class="add-panel col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Add New Student</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="index.php?page=homepage" method="POST" >
            <div class="form-group">
               <label for="st_id" class="col-md-3 control-label">Code</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="st_id" name="st_id">
               </div>
            </div>
            <div class="form-group">
               <label for="firstname" class="col-md-3 control-label">First Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="firstname" name="firstname">
               </div>
            </div>
            <div class="form-group">
               <label for="midname" class="col-md-3 control-label">Mid Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="midname" name="midname">
               </div>
            </div>
            <div class="form-group">
               <label for="lastname" class="col-md-3 control-label">Last Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="lastname" name="lastname">
               </div>
            </div>
            <div class="form-group">
               <label for="datetimepicker1" class="col-md-3 control-label">D.O.B</label>
               <div class="col-md-7 col-md-offset-2">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input type='text' class="form-control" name="birthdate" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
            <div class="form-group">
               <label for="address" class="col-md-3 control-label">Address</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="address" name="address">
               </div>
            </div>
			<div class="form-group">
               <label for="mob_no" class="col-md-3 control-label">Phone Number</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="mob_no" name="mob_no">
               </div>
            </div>
            <div class="form-group">
               <label for="dep" class="col-md-3 control-label">Department</label>
               <div class="col-md-7 col-md-offset-2">
               		<select class="form-control" id="dep" name="dep">
						<option value="Computer Science">Computer Science</option>
						<option value="Gas & Oil">Gas & Oil</option>
					</select>
               </div>
            </div>
            <div class="form-group">
            <label for="stage" class="col-md-3 control-label">Stage</label>
            <div class="col-md-7 col-md-offset-2">
               <select class="form-control" id="stage" name="stage">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
               </select>
            </div>
         </div>
         <div class="form-group">
				<label for="group" class="col-md-3 control-label">Group</label>
				<div class="col-md-7 col-md-offset-2">
					<select class="form-control" id="group" name="group">
						<option value="A">A</option>
						<option value="B">B</option>
						<option value="C">C</option>
                  <option value="D">D</option>
						<option value="E">E</option>
					</select>
				</div>
			</div>
            <div class="form-group">
               <div class="col-md-offset-2 col-md-9">
                  <button type="submit" name="add_st" class="btn btn-info actionbutton">Add Student</button>
                  <a href="index.php?page=homepage" class="btn btn-warning actionbutton">Cancel</a>
               </div>
            </div>
         </form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {
        $(".fee_disc").on('change keyup paste', function(e){
        	$("#totalfee").val( $("#fee").val() - $("#discount").val());
    	});
    });
</script>