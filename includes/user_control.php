<?php  
if ($_SESSION['admin']==1) {
$err_msg = "";
	if (isset($_POST['edit_usr'])) {
		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";

		$u_id = $_POST['u_id'];
		$full_name = safe(trim($_POST['full_name']));
		$username = safe(trim($_POST['username']));
		$role = $_POST['role'];
		if (isset($_POST['pws_chng']) && is_numeric($_POST['pws_chng'])) {
			if ($_POST['new_pass'] == $_POST['new_pass2']) {

				$newpassword = safe(trim($_POST['new_pass']));
				$salt = generateSalt();
				$password = sha1(md5($newpassword).$salt);
				$query = "UPDATE user SET
							full_name='{$full_name}', username='{$username}', 
							password='{$password}', salt='{$salt}', role={$role}
						WHERE u_id = {$u_id}";
				mysql_query($query) or die(" Error updating user data. " . mysql_error());
			}else{
				$err_msg = "<b>cannot be updated, wrong password confirmation</b>";
			}
		}else{
			$query = "UPDATE user SET
						full_name='{$full_name}', username='{$username}', role={$role}
					WHERE u_id = {$u_id}";
			mysql_query($query) or die(" Error updating user data. " . mysql_error());
		}
	}

	if (isset($_POST['add_usr'])) {

		$full_name = safe(trim($_POST['full_name']));
		$username = safe(trim($_POST['username']));
		$role = $_POST['role'];
		if ($_POST['new_pass'] == $_POST['new_pass_confirm']) {

			$newpassword = safe(trim($_POST['new_pass']));
			$salt = generateSalt();
			$password = sha1(md5($newpassword).$salt);
			$query = "INSERT INTO user(full_name, username, password, salt, role, view)
						VALUES('{$full_name}', '{$username}', '{$password}', '{$salt}', {$role}, 1)";
			mysql_query($query) or die(" Error adding new user . " . mysql_error());
		}else{
			$err_msg = "<b>cannot add new user, wrong password confirmation</b>";
		}
	}

?>
<div class="contact row">
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="student-table col-md-10">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-users'></span> User List
			</div>
		</div>
		<div class="panel-body flip-scroll">
			<table class="student-table table-bordered table-striped table-condensed flip-content">
			<caption><?php 
							if (isset($err_msg)) {
								echo $err_msg;
								$err_msg = "";
								unset($err_msg);
							}

						?></caption>
			<thead class="flip-content">
			<tr>
				<th>
					 #
				</th>
				
				<th>
					full name
				</th>
				<th>
					 username
				</th>
				<th>
					 role
				</th>
				
				<th >
					 tools
				</th>
			</tr>
			</thead>
			<tbody>
<?php
	
	$side_counter = 1;
	
	$output = "";
	
	$user_set = get_users();
	$user_set_modal = get_users();
	while ($user = mysql_fetch_assoc($user_set)) {
		if($user['view']==0)
   		{
	   		$icon = 'fa fa-check-square-o';
	   		$class = 'btn default btn-xs green user_status';
	   		$txt = 'activate';
   		}else{
	   		$icon = 'fa fa-trash-o';
	   		$class = 'btn default btn-xs red user_status';
	   		$txt = 'deactivate';
   		}
		$output .= "<tr id='{$user['u_id']}'>
						<td>
							{$side_counter}
						</td>
						
						<td>
							 {$user['full_name']}
						</td>
						<td class='numeric'>
							 {$user['username']}
						</td>
						
						<td class='numeric'>
							 <span title='1:full admin, 2:spend expense, 3:student fee, 4:view students'>
							 	{$user['role']}
							 </span>
						</td>
						<td>
							 <a href='javascript:;' role='button' id='b{$user['u_id']}' data-status='{$user['view']}' class='$class'>
								<i class='$icon'></i> <span>$txt</span></a>
							<a class='btn default btn-xs purple' data-backdrop='static' data-toggle='modal' href='#basic{$user['u_id']}'>
								<i class='fa fa-edit'></i> edit </a>
							<!-- <a href='javascript:;' class='btn default btn-xs blue'>
								<i class='fa fa-share'></i> extra </a> -->
						</td>
					</tr>";
					$side_counter++;
	}
	echo $output;
?>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	<div class="col-md-2">

	</div>
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-primary" data-backdrop='static' data-toggle='modal' href='#new_user'>
			<i class="fa fa-plus-square"></i> add new user</a>
		</div>
	</div>
</div>
<div class='modal fade' id='new_user' tabindex='-1' role='basic' aria-hidden='true'>
	<div class='modal-dialog'>
	<div class='modal-content'>
		<div class='modal-header'>
			<h4 class='modal-title'>User Information</h4>
		</div>
		<div class='modal-body modal-body-scroll'>
		
			<form class='form-horizontal' name="formadd" action='' method='POST' onsubmit="return checkpass()">
				<div class='form-group'>
					<label for='full_name' class='col-md-3 control-label'>full name:</label>
					<div class='col-md-9'>
						<input name='full_name' required='required' type='text' class='form-control'>
					</div>
				</div>

				<div class='form-group'>
					<label for='username' class='col-md-3 control-label'>user name:</label>
					<div class='col-md-9'>
						<input name='username' required='required' type='text' class='form-control'>
					</div>
				</div>

			   <div class='form-group'>
			      <label for='role' required='required' class='col-md-3 control-label'>role:</label>
			      <div class='col-md-9'>
			      	<select type='text' class='form-control input-sm' name='role'>
						<option value='1'>1</option>
						<option value='2'>2</option>
						<option value='3'>3</option>
						<option value='4'>4</option>
					</select>
			      </div>
			   </div>

				<div class='form-group'>
					<label for='new_pass' class='col-md-3 control-label'>new password:</label>
					<div class='col-md-9'>
						<input name='new_pass' type='password' class='form-control' placeholder="Password" required>
					</div>
				</div>
				<div class='form-group'>
					<label for='new_pass_confirm' class='col-md-3 control-label'>confirm password:</label>
					<div class='col-md-9'>
						<input name='new_pass_confirm' type='password' class='form-control' placeholder="Verify Password" required>
					</div>
				</div>
		
		</div>
		<div class='modal-footer'>
			<div class='form-group col-md-4'>
					<button class='form-control btn default' data-dismiss='modal' aria-hidden='true'>cancel</button>
		    </div>
		    <div class='form-group col-md-4'>
				<button type='submit' name='add_usr' class='form-control btn blue'>Add</button>
		    </div>
		</div>
			</form>
	</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<?php 
$modal="";
while ($user = mysql_fetch_assoc($user_set_modal)) {
	$selected1 = $user['role']=='1'?"selected":"";
	$selected2 = $user['role']=='2'?"selected":"";
	$selected3 = $user['role']=='3'?"selected":"";
	$selected4 = $user['role']=='4'?"selected":"";
	// $selected5 = $user['role']=='5'?"selected":"";
 	$modal .= "
				<div class='modal fade' id='basic{$user['u_id']}' tabindex='-1' role='basic' aria-hidden='true'>
				<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h4 class='modal-title'>User Information</h4>
					</div>
					<div class='modal-body modal-body-scroll'>
					
						<form class='form-horizontal' action='' method='POST' role='form'>
						 	<input type='hidden' name='u_id' value='{$user['u_id']}' />
							<div class='form-group'>
								<label for='full_name' class='col-md-3 control-label'>full name:</label>
								<div class='col-md-9'>
									<input name='full_name' id='full_name{$user['u_id']}' required='required' value='{$user['full_name']}' type='text' class='form-control'>
								</div>
							</div>

							<div class='form-group'>
								<label for='username' class='col-md-3 control-label'>user name:</label>
								<div class='col-md-9'>
									<input name='username' id='username{$user['u_id']}' value='{$user['username']}' required='required' type='text' class='form-control'>
								</div>
							</div>

						   <div class='form-group'>
						      <label for='role' required='required' class='col-md-3 control-label'>role:</label>
						      <div class='col-md-9'>
						      	<select type='text' class='form-control input-sm' id='role{$user['u_id']}' name='role'>
									<option {$selected1} value='1'>1</option>
									<option {$selected2} value='2'>2</option>
									<option {$selected3} value='3'>3</option>
									<option {$selected4} value='4'>4</option>
								</select>
						      </div>
						   </div>

						   <div class='form-group col-md-4'>

						   </div>
						   <div class='form-group col-md-8'>
								<label class='checkbox-inline'>
									<input type='checkbox' name='pws_chng' id='pws_chng{$user['u_id']}' class='pws_chng' value='1'> change password
								</label>
							</div>

							<div class='form-group new_pws' style='display:none'>
								<label for='new_pass' class='col-md-3 control-label'>new password:</label>
								<div class='col-md-9'>
									<input name='new_pass' id='new_pass{$user['u_id']}' type='password' class='on_off_req form-control'>
								</div>
							</div>
							<div class='form-group new_pws' style='display:none'>
								<label for='new_pass2' class='col-md-3 control-label'>confirm password:</label>
								<div class='col-md-9'>
									<input name='new_pass2' id='new_pass2{$user['u_id']}' type='password' class='on_off_req form-control'>
								</div>
							</div>

							
						   
					
					</div>
					<div class='modal-footer'>
						<div class='form-group col-md-4'>
								<button class='form-control btn default' data-dismiss='modal' aria-hidden='true'>cancel</button>
					    </div>
					    <div class='form-group col-md-4'>
							<button type='submit' id='edit_usr{$user['u_id']}' name='edit_usr' class='form-control btn blue'>Save</button>
					    </div>
					</div>
						</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		";
	}
	echo $modal;
?>


<script>
	jQuery(document).ready(function() {
		var on_off = false;
		$('.pws_chng').change(function()
		{
			$(".new_pws").slideToggle();
			on_off = !on_off;
			$(".on_off_req").attr("required",on_off);
		});

		//deactivate-activate user
		$('.user_status').click(function()
		{
			var status = $(this).attr('data-status');
			var theButton_id = $(this).attr('id');
			var msg = status==1?" Are you sure to deactivate this user?":" Are you sure to activate this user?";
			if (confirm(msg))
			{
				var id = $(this).parent().parent().attr('id');
				var data = 'usr_de_or_activate_id=' + id +'&current_status='+status ;
				// console.log(data);
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/user_status.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
				   		if(status==1)
				   		{
					   		var new_icon = 'fa fa-check-square-o';
					   		var new_class = 'btn default btn-xs green user_status';
					   		var new_txt = 'activate';
				   		}else{
					   		var new_icon = 'fa fa-trash-o';
					   		var new_class = 'btn default btn-xs red user_status';
					   		var new_txt = 'deactivate';
				   		}

						$("#"+theButton_id).attr('data-status', data1);
						$("#"+theButton_id).attr('class', new_class);
						$("#"+theButton_id+">i").attr('class', new_icon);
						$("#"+theButton_id+">span").text(new_txt);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});

	});

function checkpass() {
	var p1 = document.forms["formadd"]["new_pass"].value;
	var p2 = document.forms["formadd"]["new_pass_confirm"].value;
    if (p1 != p2 || p1.length<8) {
        alert("you can't continue, either password mismatch or length < 8");
        return false;
    }
}
</script>


<?php
}else{
	echo "<p class='alert alert-danger'>YOU ARE NOT ALLOWED TO SEE THIS PAGE<p>";
}

?>