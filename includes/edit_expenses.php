<?php  
if ($_SESSION['admin']==1 || $_SESSION['admin']==2)
{
	if (isset($_POST['add_edit_expense'])) {
		$act = "add";
		if (isset($_GET['act']) && !empty($_GET['act'])) {
			$act = $_GET['act'];
		}
		$ex_name = safe(trim($_POST['ex_name']));
   		$ex_order = safe(trim($_POST['ex_order']));

   		if ($_GET['act'] == "add") {
   			$query = "INSERT INTO expense_name(name, `order`)";
         	$query .= " VALUES('{$ex_name}', {$ex_order} )";
         	mysql_query($query) or die("ERROR adding new Expense " . mysql_error());
   		}elseif ($_GET['act'] == "edit" && isset($_POST['ex_id'])) {
         	$query = "UPDATE expense_name SET name = '{$ex_name}', `order` = {$ex_order} WHERE id = {$_POST['ex_id']}";
         	mysql_query($query) or die("ERROR Updating Expense " . mysql_error());
   		}
	}
?>
<div class="contact row">
	<div class="add-panel col-md-7">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> List of Expenses</h3>
			</div>
			<div class="panel-body">
			<table class="col-md-12 table-bordered table-striped table-condensed table-responsive" style="font-size: 110%;">
		<thead>
		<tr>
			<th>#</th>
			<th>expense name</th>
			<th>order</th>
			<th>tools</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$expense_names_set = get_expense_names();
			$output = "";
			$i = 1;
			while ($expense_names = mysql_fetch_assoc($expense_names_set)) {
				$output .= "<tr id='{$expense_names['id']}'> 
								<td>{$i}</td>
								<td>{$expense_names['name']}</td>
								<td>{$expense_names['order']}</td>
								<td class='edit_expenses'>
								 <a href='javascript:;' role='button' class='btn default btn-xs red delete_fee'>
										<i class='fa fa-trash-o'></i> delete </a>
									<a href='index.php?page=edit_expenses&act=edit&ex_type={$expense_names['id']}' class='btn default btn-xs blue'>
										<i class='fa fa-edit'></i> edit </a>
								</td>
							</tr>";
							$i++;
			}
			echo $output;
		?>
		</tbody>
		</table>
			</div>
		</div>
		<a style="float:right" href='index.php?page=edit_expenses&act=add' class='btn default blue'>
			<i class='fa fa-plus'></i> Add New Expense </a>
	</div>
</div>





<?php

//load form
  if (isset($_GET['act']) && !empty($_GET['act']) && !isset($_GET['noshow'])) {
  	$ex_type = 0;
	$act = "add";
    if (isset($_GET['ex_type']) && !empty($_GET['ex_type'])) {
    	$ex_type = $_GET['ex_type'];
    	$act = "edit";
    }
    
    $ex_id = "";
    $name = "";
	$order = "";
    if ($ex_type>0) {
    	$expense_names_set = get_expense_names($ex_type);
    	$expense_names = mysql_fetch_assoc($expense_names_set);
    	$ex_id = $expense_names['id'];
    	$name = $expense_names['name'];
    	$order = $expense_names['order'];
    }
   
?>

<div class="row">
	<div class="add-panel col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Edit or Add Expense Type</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="index.php?page=edit_expenses&act=<?php echo $act; ?>&noshow=1" method="POST" >
            <div class="form-group">
               <label for="ex_name" class="col-md-3 control-label">Expense Name</label>
               <div class="col-md-7">
                  <input type="text" required="required" class="form-control" id="ex_name" name="ex_name" value="<?php echo $name; ?>">
                  <input type="hidden" class="form-control" id="ex_id" name="ex_id" value="<?php echo $ex_id; ?>">
               </div>
            </div>            
            
            <div class="form-group">
               <label for="ex_order" class="col-md-3 control-label">Order</label>
               <div class="col-md-7">
                  <input type="number" required="required" class="form-control" id="ex_order" name="ex_order" value="<?php echo $order; ?>">
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-9">
                  <button type="submit" name="add_edit_expense" class="btn btn-info">save</button>
                  <a href="index.php?page=edit_expenses" class="btn btn-warning">cancel</a>
               </div>
            </div>
         </form>
			</div>
		</div>
	</div>
</div>
<?php
}
?>

<?php
   }//Admin role IF
?>