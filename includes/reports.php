<?php 
if ($_SESSION['admin']==1 || $_SESSION['admin']==3)
{
	$given_fee = given_fee();
	$total_fee = total_fee();
	$remaining_fee = $total_fee - $given_fee;
 ?>
 <style type="text/css">
.labelspan{
	font-weight: bolder;
}
.dataspan{
	font-weight: bolder;
}

.date_on_off{
	display: none;
}

 </style>

<?php  
if ($_SESSION['admin']==1)
{
?>
<div class="contact row">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-money"></i> Money of Institute</h3>
			</div>
			<div class="panel-body flip-scroll">
				<table class="table table-striped flip-content" style="font-size: 110%;">
					<thead class="flip-content">
						<tr>
							<th>total payed fee</th>
							<th>total remaining fee</th>
							<th>total fee</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $given_fee; ?></td>
							<td><?php echo $remaining_fee; ?></td>
							<td><?php echo $total_fee; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php  
} // only full admin can see this
?>
<div class="contact row">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user"></i> Student Payed Fee</h3>
			</div>
			<div class="panel-body">
			<div class="col-md-6">
				<form class="form-horizontal" role="form" action="index.php?page=reports" method="POST" >

            <div class="form-group">
               <label for="name_code" class="col-md-3 control-label">name and code of Student</label>
               <div class="col-md-7">
                  <select class="select2 input-default" id="name_code" name="code">
                  <?php

                  	$deleted_st = "";
                  	if (isset($_GET['deleted']) && !empty($_GET['deleted']) && is_numeric($_GET['deleted']) && is_numeric($_GET['deleted'])>0 ) {
                  		$deleted_st = $_GET['deleted'];
                  	}


                  	$st_code="";
                  	if (isset($_POST['student_report']) && isset($_POST['code']) && !empty($_POST['code'])  ) {
                  		$st_code = $_POST['code'];
                  	}
                  	elseif(isset($_GET['st_id']) && !empty($_GET['st_id'])){
						$st_code = $_GET['st_id'];
                  	}
                  	if (!empty($deleted_st) && is_numeric($deleted_st) && $deleted_st>0) {
	                    $student_names_set = get_students(-1,-1,0,0,0,0, 1);
                  	}else{
						$student_names_set = get_students();
                  	}

                     while ($student_names = mysql_fetch_assoc($student_names_set)) {
                     	$selected = "";
                     	$selected = $st_code==$student_names['st_id']?"selected":"";
                        echo "<option {$selected} value='{$student_names['st_id']}'>{$student_names['st_id']},  
	                        {$student_names['fname']} {$student_names['mname']} {$student_names['lname']}
	                        </option>";
                     }
                     $first_st_id = isset($st_code) && !empty($st_code)?$st_code:mysql_result($student_names_set, 0, 'st_id');
                  ?>
                  </select>
                  	
               </div>
            </div>
            
            <div class="form-group">
               <label for="fee_type" class="col-md-3 control-label">Fee Type</label>
               <div class="col-md-7">
                  <select required="required" class="select2 input-default" id="fee_type" name="owed_id">
                  <?php 
					$first_owed_total_fee="";
					$first_owed_discount_fee="";
					$first_fee_title="";
                  $owed_id="";
                  	if (isset($_POST['student_report']) && isset($_POST['owed_id']) && !empty($_POST['owed_id'])  ) {
                  		$owed_id = $_POST['owed_id'];
                  	}
                  	elseif(isset($_GET['owed_id']) && !empty($_GET['owed_id'])){
						$owed_id = $_GET['owed_id'];
                  	}
					$st_owed_fee_list_set = get_st_owed_fee_list($first_st_id, 1);
					while ($st_owed_fee_list = mysql_fetch_assoc($st_owed_fee_list_set)) {
						$selected = $st_owed_fee_list['owed_id'] == $owed_id?"selected":"";
						echo "<option {$selected} value='{$st_owed_fee_list['owed_id']}'>{$st_owed_fee_list['fee_title']} - {$st_owed_fee_list['year']}</option>";
						if (isset($selected) && !empty($selected)) {
							$first_owed_total_fee = $st_owed_fee_list['total_fee'];
							$first_owed_discount_fee = $st_owed_fee_list['discount'];
							$first_fee_title = $st_owed_fee_list['fee_title'];
						}
					}
					$first_owed_id = isset($owed_id) && !empty($owed_id)?$owed_id:mysql_result($st_owed_fee_list_set, 0, 'owed_id');
					$first_owed_total_fee = isset($first_owed_total_fee) && !empty($first_owed_total_fee)?$first_owed_total_fee:mysql_result($st_owed_fee_list_set, 0, 'total_fee');
					$first_owed_discount_fee = isset($first_owed_discount_fee) && !empty($first_owed_discount_fee)?$first_owed_discount_fee:mysql_result($st_owed_fee_list_set, 0, 'discount');
					$first_fee_title = isset($first_fee_title) && !empty($first_fee_title)?$first_fee_title:mysql_result($st_owed_fee_list_set, 0, 'fee_title');
					//st_owed_fee_list['total_fee']
                  ?>
                  </select>
                  <br />
                  <button style="margin-top:10px;" type="submit" name="student_report" class="btn btn-success btn-sm">ok</button>
               </div>
            </div>
         </form>
         </div>
         <div class="col-md-6">
         	<form class="form-horizontal" role="form" action="index.php?page=conc_report" method="POST" >
         	<!-- conclusion report -->
            <div class="form-group">
               <label for="dep" class="col-md-3 control-label">Department</label>
               <div class="col-md-7">
                  <select class="select2 input-default" id="dep" name="dep">
                  	<option value="0">both</option>
                  	<option value="Computer Science">Computer Science</option>
                  	<option value="Gas & Oil">Gas & Oil</option>
                  </select>
                  	
               </div>
            </div>
            <div class="form-group">
               <label for="conc_stage" class="col-md-3 control-label">Stage</label>
               <div class="col-md-7">
                  <select class="select2 input-default" id="conc_stage" name="conc_stage">
                  	<option value="0">all</option>
                  	<option selected="selected" value="1">1</option>
                  	<option value="2">2</option>
                  	<option value="3">3</option>
                  	<option value="4">4</option>
                  	<option value="5">5</option>
                  </select>
                  	
               </div>
            </div>
            <div class='form-group col-md-push-3 col-md-10'>
				<label class='checkbox-inline'>
					<input type="checkbox" name="date_on" id="date_on" value="1" > set paying date
				</label>
			</div>
            <div class="form-group date_on_off">
               <label for="datetimepicker1" class="col-md-3 control-label">from</label>
               <div class="col-md-7">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input type='text' class="form-control on_off_req" name="from" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
            <div class="form-group date_on_off">
               <label for="datetimepicker1" class="col-md-3 control-label">to</label>
               <div class="col-md-7">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input type='text' class="form-control on_off_req" name="to" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
            <div class="form-group">
               <label for="fee_type" class="col-md-3 control-label">Fee Type</label>
               <div class="col-md-7">

                  <select required="required" class="select2 input-default" id="fee_type" name="fee_type">
                  <?php 
            		$get_fee_types_set = get_fee_types();
					
					while ($fee_types = mysql_fetch_assoc($get_fee_types_set)) {
						$selected = ""; //$st_owed_fee_list['owed_id'] == $owed_id?"selected":"";
						echo "<option {$selected} value='{$fee_types['fee_title']} - {$fee_types['year']}'>{$fee_types['fee_title']} - {$fee_types['year']}</option>";
					}
					
                  ?>
                  </select>
                  <br />
                  <button style="margin-top:10px;" type="submit" name="conc_report" class="btn btn-info btn-sm">ok</button>
               </div>
            </div>
         </form>
         </div>
         <hr />
<?php 
	//student report form
	if ((isset($_POST['student_report']) && isset($_POST['code']) && !empty($_POST['code'])) || (isset($_GET['st_id']) && !empty($_GET['st_id'])  )) {
		$student_info = mysql_fetch_assoc( (!empty($deleted_st) && is_numeric($deleted_st) && $deleted_st>0)?get_students(-1,-1, $st_code,0,0,0,1):get_students(-1,-1, $st_code) );
		$payed_fee = get_st_payed_fee($first_owed_id);
		$student_invoice_set = student_invoice($first_owed_id);
	
?>

	<div class="col-md-2" style="text-align: right;">
		<span class="labelspan1">Name: </span> <br>
		<span class="labelspan1">Address: </span> <br>
		<span class="labelspan1">Phone no.: </span> <br>
		<span class="labelspan1">Department and Stage: </span> <br>
		<span class="labelspan1"><?php echo $first_fee_title; ?>: </span> <br>
		<span class="labelspan1">Discount: </span> <br>
		<span class="labelspan1">Total Payed Fee: </span> <br>
		<span class="labelspan1">Remained Fee: </span> <br>
	</div>

	<div class="col-md-4" style="margin-bottom: 15px;">
        <span class="dataspan1"><?php echo $student_info['fname'] . " ". $student_info['mname'] . " ". $student_info['lname']; ?> </span> <br>
		<span class="dataspan1"><?php echo $student_info['address']; ?> </span> <br>
		<span class="dataspan1"><?php echo $student_info['phone_no']; ?> </span> <br>
		<span class="dataspan1"><?php echo $student_info['dep'] . ", " . $student_info['stage'];; ?> </span> <br>
		<span class="dataspan1"><?php echo $first_owed_total_fee; ?> </span> <br>
		<span class="dataspan1"><?php echo $first_owed_discount_fee; ?> </span> <br>
		<span class="dataspan1"><?php echo $payed_fee; ?> </span> <br>      
		<span class="dataspan1"><?php echo $first_owed_total_fee - $payed_fee; ?> </span>     
	</div>



	<table class="col-md-12 table-bordered table-striped table-condensed table-responsive" style="font-size: 110%;">
		<thead>
		<tr>
			<th>invoice no.</th>
			<!-- <th>ژ.قیست</th> -->
			<th>date</th>
			<th>amount</th>
			<th>note</th>
			<th>tools</th>
		</tr>
		</thead>
		<tbody>
		<?php  
			while ($student_invoice_info = mysql_fetch_assoc($student_invoice_set)) {
				echo "
					<tr id={$student_invoice_info['in_id']}>
						<td>{$student_invoice_info['invoice_number']}</td>
						
						<td>{$student_invoice_info['fee_date']}</td>
						<td>{$student_invoice_info['amount']}</td>
						<td>{$student_invoice_info['note']}</td>
						<td>
						 <a href='javascript:;' role='button' class='btn default btn-xs red delete_fee'>
								<i class='fa fa-trash-o'></i> delete </a>
							<a href='index.php?page=print_invoice&fee_id={$student_invoice_info['in_id']}&deleted={$deleted_st}' class='btn default btn-xs blue'>
								<i class='fa fa-share'></i> view </a>
						</td>
					</tr>
				";
			}
		?>
		</tbody>
		</table>
<?php
	
	}//end POST if 
?>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<script>
      $('.select2').select2({ placeholder : '' });

      $('.select2-remote').select2({ data: [{id:'A', text:'A'}]});

      $('button[data-select2-open]').click(function(){
        $('#' + $(this).data('select2-open')).select2('open');
      });
</script>

<script>
	jQuery(document).ready(function() {
		var on_off = false;
		$('#date_on').change(function()
		{
			$(".date_on_off").slideToggle();
			on_off = !on_off;
			$(".on_off_req").attr("required",on_off);
		});

	});


</script>


<?php
}//admin role IF
?>