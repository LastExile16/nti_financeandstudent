<style type="text/css">
	
@media print{
	.top-header, .copywrite, .top-menu, .print-button, .noprint{
		display: none;
	}
	.panel-body{
		margin-top: 2cm;
	}
	table{
		font-size: 50%;
	}
	#para_wargrtn{
		margin-left: 8em !important;
	}
}

</style>
<?php 
	$given_fee = given_fee();
	$total_fee = total_fee();
	$remaining_fee = $total_fee - $given_fee;
 ?>
 <style type="text/css">
.labelspan{
	font-weight: bolder;
}
.dataspan{
	font-weight: bolder;
}
.no_payed_fee{
	background-color: red !important;
    color: white;
}

 </style>
<div class="contact row noprint">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-money"></i> Money of Institute</h3>
			</div>
			<div class="panel-body flip-scroll">
				<table class="table table-striped flip-content" style="font-size: 110%;">
					<thead class="flip-content">
						<tr>
							<th>total payed fee</th>
							<th>total remaining fee</th>
							<th>total fee</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $given_fee; ?></td>
							<td><?php echo $remaining_fee; ?></td>
							<td><?php echo $total_fee; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="contact row">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-success">
			<div class="panel-heading">
			<?php  
				if (isset($_POST['conc_report'])) {
					$dep = $_POST['dep'];
					$stage = $_POST['conc_stage'];
					$fee_type = $_POST['fee_type'];
					$from = isset($_POST['date_on']) && $_POST['date_on']==1?$_POST['from']:-1;
					$to = isset($_POST['date_on']) && $_POST['date_on']==1?$_POST['to']:-1;
			?>
				<h3 class="panel-title"><i class="fa fa-user"></i> Stage Report of <?php echo $fee_type . " " . $dep . " From: ". $from . " - To:". $to; ?></h3>
			</div>
			<div class="panel-body">
			<div class="col-md-10">
			<table class="col-md-12 table-bordered table-striped table-condensed table-responsive" style="font-size: 110%;">
		<thead>
		<tr>
			<th>#</th>
			<th>Code</th>
			<th>Name</th>
			<th>Stage</th>
			<th>Fee</th>
			<th>Discount</th>
			<th>Total Fee</th>
			<th>payed Fee</th>
			<th>Remaining Fee</th>
		</tr>
		</thead>
		<tbody>
		<?php  
		
		$fee_type_arr = explode(" - ", $fee_type);
		$student_owed_fee_set = get_conc_report($dep, $fee_type_arr[0], $fee_type_arr[1], $stage, $from, $to);
		$counter=0;
		$not_payed_st_counter=0;
		$prev_st_id_f="";
		while ($student_owed_fee = mysql_fetch_assoc($student_owed_fee_set)) {	
			$get_st_payed_fee = get_st_payed_fee($student_owed_fee['owed_id']);
			// $get_st_payed_fee = $get_st_payed_fee_func>0?$get_st_payed_fee_func:0;
			$remained_fee = $student_owed_fee['total_fee'] - $get_st_payed_fee;
			// $no_payed = empty($get_st_payed_fee)?"class='no_payed_fee'":"";
			
			if ($student_owed_fee['st_id_f']==$prev_st_id_f) {
				continue;
			}
			$prev_st_id_f = $student_owed_fee['st_id_f'];
			
			if (empty($get_st_payed_fee)) {
				$no_payed = "class='no_payed_fee'";
				$not_payed_st_counter++;
			}
			else{
				$no_payed = "";
			}
			//$payed_fee_all = intval($get_st_payed_fee);
			// echo var_dump(empty($get_st_payed_fee));
			$counter++;
			echo "
				<tr id={$student_owed_fee['st_id_f']}>
					<td>{$counter}</td>
					<td>{$student_owed_fee['st_id_f']}</td>
					<td>{$student_owed_fee['fullname']}</td>
					<td>{$student_owed_fee['stage']} - {$student_owed_fee['group']}</td>
					<td>{$student_owed_fee['fee']}</td>
					<td>{$student_owed_fee['discount']}</td>
					<td>{$student_owed_fee['total_fee']}</td>
					<td {$no_payed}>{$get_st_payed_fee}</td>
					<td>{$remained_fee}</td>
				</tr>
						
			";
		}
		?>
		</tbody>
		
		
		</table>
		<p> total number of not payed students: 
			<?php  
				echo $not_payed_st_counter;
			?>
		</p>
			<?php  
				}//end POST if
			?>
			</div>
			</div>
		</div>
	</div>
</div>