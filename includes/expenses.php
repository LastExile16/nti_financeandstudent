<?php  
if ($_SESSION['admin']==1 || $_SESSION['admin']==2 || $_SESSION['admin']==3)
{
?>

<?php 
	$total_instit_money = get_total_money();
	$spent_money = get_spent_money();
	$total_fee = total_fee();
 ?>
 <style type="text/css">
.labelspan{
	font-weight: bolder;
}
.dataspan{
	font-weight: bolder;
}
 </style>
 <?php  
if ($_SESSION['admin']==1)
{
?>
<div class="contact row">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-money"></i> Money of Institute</h3>
			</div>
			<div class="panel-body flip-scroll">
				<table class="table table-striped flip-content" style="font-size: 110%;">
					<thead class="flip-content">
						<tr>
							<th>total available money</th>
							<th>total expended money</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $total_instit_money; ?></td>
							<td><?php echo $spent_money; ?></td>
							
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php  
} // only for full admin
?>

<div class="contact row">
	<!-- BEGIN SEARCH TABLE PORTLET-->
	<div class="search-panel col-md-10">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user"></i> Spent Expenses</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="index.php?page=expenses" method="POST" >

			<div class="form-group">
               <label for="exp_type" class="col-md-3 control-label">expense type</label>
               <div class="col-md-7">
                  <select class="select2 input-default" id="exp_type" name="exp_type">
                  <?php 
                     $expense_name_set = get_expense_names();
                     while ($expense_name = mysql_fetch_assoc($expense_name_set)) {
                        echo "<option value='{$expense_name['id']}'>{$expense_name['name']}</option>";
                     }
                  ?>
                  </select>
                  <br />
                  <button style="margin-top:10px;" type="submit" name="expense_report" class="btn btn-success btn-sm">ok</button>
                  <a style="margin-top:10px;" href="./index.php?page=edit_expenses" name="new_expense" class="btn btn-info btn-sm">edit or add new expense</a>
               </div>
            </div>

         </form>
         <hr />
<?php 
	if (isset($_POST['expense_report'])) {
?>
	<table class="col-md-12 table-bordered table-striped table-condensed table-responsive" style="font-size: 110%;">
		<thead>
		<tr>
			<th>invoice no.</th>
			<!-- <th>ژ.قیست</th> -->
			<th>date</th>
			<th>amount</th>
			<th>note</th>
			<th>tools</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$expense_report_info_set = get_expense_report_info($_POST['exp_type']);
			while ($expense_report_info = mysql_fetch_assoc($expense_report_info_set)) {
				echo "
					<tr id={$expense_report_info['ex_id']}>
						<td>{$expense_report_info['invoice_number']}</td>
						
						<td>{$expense_report_info['date']}</td>
						<td>{$expense_report_info['amount']}</td>
						<td>{$expense_report_info['note']}</td>
						<td class='expenses'>
						 <a href='javascript:;' role='button' class='btn default btn-xs red delete_fee'>
								<i class='fa fa-trash-o'></i> delete </a>
							<a href='index.php?page=print_invoice_expense&ex_id={$expense_report_info['ex_id']}' class='btn default btn-xs blue'>
								<i class='fa fa-share'></i> view </a>
						</td>
					</tr>
				";
			}
		?>
		</tbody>
		</table>
<?php
	
	}//end POST if 
?>
			</div>
		</div>
	</div>
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<script>
      $('.select2').select2({ placeholder : '' });

      $('.select2-remote').select2({ data: [{id:'A', text:'A'}]});

      $('button[data-select2-open]').click(function(){
        $('#' + $(this).data('select2-open')).select2('open');
      });
</script>



<?php  
} // admin role IF
?>