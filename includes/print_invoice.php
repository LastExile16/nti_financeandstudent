<?php
if ($_SESSION['admin']==1 || $_SESSION['admin']==3)
{
	//if request came from pay_fee.php
	if (isset($_POST['pay_fee'])) {
		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";
		$new_inv_num = max_inv_num()+1;
		$student_info = get_students(-1,-1, $_POST['code']);
		$query = "INSERT INTO invoices(invoice_number, st_owed_fee_id_f, amount, fee_date, note, user_id_f) VALUES
					({$new_inv_num}, {$_POST['fee_type']}, {$_POST['amount']}, '{$_POST['qist_date']}', '{$_POST['note']}', {$_SESSION['user_id']})";
		mysql_query($query) or die("Q1.printingInvoice".mysql_error());
		if ($_POST['amount'] === $_POST['remaining_fee']) {
			$query = "UPDATE st_owed_fee SET payment_completed=1 WHERE st_owed_fee_id = {$_POST['fee_type']}";
			mysql_query($query) or die("Q2.completeInvoice".mysql_error());
		}
		$invoice_code = mysql_insert_id();
		$student = mysql_fetch_assoc($student_info);

		$qist_date = $_POST['qist_date'];
		$amount = $_POST['amount'];
		$note = $_POST['note'];
		$accountant = $_SESSION['fullname'];

	}
	else{

		$deleted_st = false;
      	if (isset($_GET['deleted']) && !empty($_GET['deleted']) && is_numeric($_GET['deleted']) ) {
      		$deleted_st = true;
      	}

		//request came from reports.php
		$in_id = 0;
		if (isset($_GET['fee_id'])) {
			$in_id = $_GET['fee_id'];
		}

		$student_invoice_set = student_invoice(0, $in_id);
		$student_invoice = mysql_fetch_assoc($student_invoice_set);
		// echo "<pre>";
		// print_r($student_invoice);
		// echo "</pre>";
		 /*[in_id] => 1
	    [invoice_number] => 1
	    [fee_date] => 2015-11-07
	    [amount] => 900000
	    [note] => qisti yakamiati
	    [user_id_f] => 1*/
		$student_info = $deleted_st==true?get_st_info_on_inv_id($student_invoice['in_id'],1):get_st_info_on_inv_id($student_invoice['in_id']);
		$student = mysql_fetch_assoc($student_info);

		$qist_date = $student_invoice['fee_date'];
		$amount = $student_invoice['amount'];
		$note = $student_invoice['note']; 
		$accountant = get_invoice_accountant($student_invoice['user_id_f']);
		$invoice_code = $student_invoice['invoice_number'];

	}
?>
<style type="text/css">
	@media print{
		.top-header, .copywrite, .top-menu, .print-button{
			display: none;
		}
		.panel-body{
			margin-top: 2cm;
		}
		table{
			font-size: 50%;
		}
		#para_wargrtn{
		margin-left: 8em !important;
	}
	}
	.to_right{
		float: right;
	}
	.pad_it{
		padding-bottom: 15em !important;
		padding-top: 2em !important;
	}
	table{
		table-layout:fixed;
		word-wrap:break-word;
	}
	.inv_header{
		margin-bottom: 20px;
		/*border: 1px solid;*/
	}
	#para_wargrtn{
		margin-left: 15em;
	}
</style>
<div class="row">
	<div class="col-md-9 col-sm-12 col-xs-12 student-table">
		<div class="panel-body flip-scroll">
		<div class="row inv_header">
			<div class="col-md-3 col-sm-3 col-xs-3">
				<span id="left_logo"><img src="./images/logo.jpg" height="100" width="100"></span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6" id="center_txt">
				<p >National Technical institute</p>
				<p>پەیمانگەی نیشتیمانی تەکنیکی نێودەوڵەتی</p>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3">
				<span id="right_logo" style="float: right;"><img src="./images/logo_krg.png" height="100" width="125"></span>
			</div>
		</div>
		<caption><span class="invoice_code">invoice no.: <?php echo $invoice_code; ?></span><span id="para_wargrtn">پسوولەی پارە وەرگرتن</span><span class="to_right">date: <?php echo $qist_date; ?></span></caption>
			<table class="table-bordered table-striped table-condensed flip-content">
			<thead class="flip-content">
			<tr>
				<th>
					 name and code of student
				</th>
				<th>
					 department and stage
				</th>
				<th>
					 amount
				</th>
				<th>
					 note
				</th>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$output = "<tr id='{$student['st_id']}'>
							<td class='pad_it'>
								{$student['st_id']}, {$student['fname']} {$student['mname']} {$student['lname']}
							</td>
							<td class='pad_it'>
								 {$student['dep']} - {$student['stage']}
							</td>
							<td class='pad_it'>
								{$amount}
							</td>
							<td class='pad_it'>
								 {$note}
							</td>
						</tr>";			
			echo $output;
		?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">accountant name: <br/> <?php echo $accountant; ?></td>
				</tr>
			</tfoot>
			</table>
		</div>
	</div>
	
</div>
<div class="row print-button">
   <div class="col-md-9 col-sm-12 col-xs-12" style="float: right; margin-right: 15px;">
      <a href="javascript:window.print();" class="btn btn-defualt btn-lg red"><i class="glyphicon glyphicon-print"></i></a>
   </div>
</div>
<?php 
} // admin role IF
?>

