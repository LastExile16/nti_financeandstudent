<?php
   $st_id = isset($_GET['st_id']) && !empty($_GET['st_id'])?$_GET['st_id']:0;
   $students_set = get_students(-1,-1, $st_id);
   if (mysql_num_rows($students_set)>0) {
      $student = mysql_fetch_assoc($students_set);
      
   
?>

<div class="contact row">
	<div class="add-panel col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Edit Student Data</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" action="index.php?page=homepage" method="POST" >
            <div class="form-group">
               <label for="st_id" class="col-md-3 control-label">Code</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="st_id" name="st_id" value="<?php echo $student['st_id']; ?>">
                  <input type="hidden" class="form-control" id="st_id" name="old_st_id" value="<?php echo $student['st_id']; ?>">
               </div>
            </div>
            <div class="form-group">
               <label for="firstname" class="col-md-3 control-label">First Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="firstname" name="firstname" value="<?php echo $student['fname']; ?>">
               </div>
            </div>
            <div class="form-group">
               <label for="midname" class="col-md-3 control-label">Mid Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="midname" name="midname" value="<?php echo $student['mname']; ?>">
               </div>
            </div>
            <div class="form-group">
               <label for="lastname" class="col-md-3 control-label">Last Name</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="lastname" name="lastname" value="<?php echo $student['lname']; ?>">
               </div>
            </div>
            <div class="form-group">
               <label for="datetimepicker1" class="col-md-3 control-label">D.O.B</label>
               <div class="col-md-7 col-md-offset-2">
	               <div class='input-group date datepick' id='datetimepicker1'>
						<input type='text' class="form-control" name="birthdate" value="<?php echo $student['dob']; ?>">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
               		</div>
               </div>
            </div>
            <div class="form-group">
               <label for="address" class="col-md-3 control-label">Address</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="address" name="address" value="<?php echo $student['address']; ?>">
               </div>
            </div>
			<div class="form-group">
               <label for="mob_no" class="col-md-3 control-label">Phone Number</label>
               <div class="col-md-7 col-md-offset-2">
                  <input type="text" required="required" class="form-control" id="mob_no" name="mob_no" value="<?php echo $student['phone_no']; ?>">
               </div>
            </div>
            <div class="form-group">
               <label for="dep" class="col-md-3 control-label">Department</label>
               <div class="col-md-7 col-md-offset-2">
                  <select class="form-control" id="dep" name="dep">
                     <option <?php echo $student['dep']=='Computer Science'?'selected':'' ?> value="Computer Science">Computer Science</option>
                     <option <?php echo $student['dep']=='Gas & Oil'?'selected':'' ?> value="Gas & Oil">Gas & Oil</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
            <label for="stage" class="col-md-3 control-label">Stage</label>
            <div class="col-md-7 col-md-offset-2">
               <select class="form-control" id="stage" name="stage">
               <?php
                  for ($i=1; $i <= 5; $i++) { 
                     $selected_option = $student['stage']==$i?"selected":"";
                     echo "<option {$selected_option} value='{$i}'>{$i}</option>";
                  }
               ?>
               </select>
            </div>
         </div>
         <div class="form-group">
				<label for="group" class="col-md-3 control-label">Group</label>
				<div class="col-md-7 col-md-offset-2">
					<select class="form-control" id="group" name="group">
               <?php
                  $group_arr = array('A','B','C','D','E');
                  foreach ($group_arr as $grp) {
                     $selected_option = $student['group']==$grp?"selected":"";
                     echo "<option {$selected_option} value='{$grp}'>{$grp}</option>";
                  }
                  
               ?>
					</select>
				</div>
			</div>

            <div class="form-group">
               <div class="col-md-offset-2 col-md-9">
                  <button type="submit" name="edit_st" class="btn btn-info actionbutton">save</button>
                  <a href="index.php?page=homepage" class="btn btn-warning actionbutton">cancel</a>
               </div>
            </div>
         </form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {
        $(".fee_disc").on('change keyup paste', function(e){
        	$("#totalfee").val( $("#fee").val() - $("#discount").val());
    	});
    });
</script>

<?php
   }//end of num_rows if statement
?>