<?php 
if(!isset($_SESSION))
{
	session_start();	
}
require_once('./connection.php');
require_once('./functions.php');
if(logged_in())
{
	if(isset($_POST['st_owed_fee_id']))
	{
		$st_owed_fee_list_set = get_st_total_payed_remaining_fee($_POST['st_owed_fee_id']);
		$st_owed_fee_list = mysql_fetch_assoc($st_owed_fee_list_set);
		echo json_encode( 
			array("owed_fee" => "{$st_owed_fee_list['owed_fee']}",
			"payed_fee" => "{$st_owed_fee_list['payed_fee']}")
			);
	}
}
else
{
	header('Location:../index.php');
	exit;
}
?>