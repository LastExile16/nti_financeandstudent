<?php
if(!isset($_SESSION))
{
    session_start();	
}
require_once("./includes/functions.php");

			// Four steps to closing a session
			// (i.e. logging out)			

			
			// 2. Unset all the session variables
			$_SESSION = array();
			
			//3. Destroy the session cookie
			if(isset($_COOKIE[session_name()])){
				setcookie(session_name(), '', time()-42000, '/'); // '/' at root level
			}
			
			// 4. Destroy the session
			session_destroy();
			
			header("Location: index.php?loggedout=1");
			exit;

?>