-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2015 at 06:58 PM
-- Server version: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nishtiman_instit`
--

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE IF NOT EXISTS `fee` (
  `fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id_f` varchar(50) NOT NULL,
  `qist_count` tinyint(3) unsigned NOT NULL,
  `amount` mediumint(8) unsigned NOT NULL,
  `fee_date` date NOT NULL,
  `note` text,
  `view` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`fee_id`),
  KEY `st_id_f` (`st_id_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`fee_id`, `st_id_f`, `qist_count`, `amount`, `fee_date`, `note`, `view`) VALUES
(1, 'CS1', 1, 125000, '2015-10-11', '', 1),
(2, 'CS2', 1, 150000, '2015-10-06', 'parai yakam', 1),
(3, 'CS1', 2, 200000, '2015-10-15', NULL, 1),
(4, 'CS1', 3, 125000, '2015-10-20', NULL, 1),
(5, 'CS2', 2, 350000, '2015-10-10', NULL, 1),
(6, 'CS1', 1, 5828258, '2015-10-20', 'جەند تێبینیەک جەند تێبینیەک  جەند تێبینیەک  جەند تێبینیەک  جەند تێبینیەک ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `st_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'date of birth',
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `dep` varchar(40) NOT NULL COMMENT 'departmant',
  `stage` varchar(15) NOT NULL,
  `fee` bigint(20) unsigned NOT NULL,
  `discount` int(255) unsigned NOT NULL DEFAULT '0',
  `total_fee` bigint(20) unsigned NOT NULL,
  `view` tinyint(4) DEFAULT '1' COMMENT '1:visible, 0:deleted',
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`st_id`, `fname`, `mname`, `lname`, `dob`, `address`, `phone_no`, `dep`, `stage`, `fee`, `discount`, `total_fee`, `view`) VALUES
('CS1', 'ئازاد', 'هێمن', 'جەلیل', '1994-02-27', 'هەولێر', '0750854123', 'کۆمپیوتەر', '2', 1800000, 800000, 1000000, 1),
('CS2', 'هەژار', 'قادر', 'غەریب', '2015-10-05', 'سلێمانی', '07505471258', 'نەوت', '1', 2500000, 0, 2500000, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fee`
--
ALTER TABLE `fee`
  ADD CONSTRAINT `relation` FOREIGN KEY (`st_id_f`) REFERENCES `student` (`st_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
