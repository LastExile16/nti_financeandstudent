# README #

Student and finance management web application for National technical institute (private school)

### What is this repository for? ###

* This repository contains project files for a web application built with HTML, CSS, JS
* The server side scripting language is PHP
* the database that stores information build with MySQL

### How do I access the test page? ###
* test page can be accessed through nti.nawrascs.com/login.php
* username: test
* password: nawras

### Who do I talk to? ###

* Repo owner