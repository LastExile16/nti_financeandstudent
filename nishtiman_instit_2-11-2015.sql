-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2015 at 07:49 PM
-- Server version: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nishtiman_instit`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `ex_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `e_id_f` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `user_id_f` varchar(255) DEFAULT NULL COMMENT 'manager',
  `invoice_number` int(10) unsigned DEFAULT NULL,
  `view` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ex_id`),
  KEY `e_id_f` (`e_id_f`),
  KEY `user_id_f` (`user_id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `expense_name`
--

CREATE TABLE IF NOT EXISTS `expense_name` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'expense name',
  `view` tinyint(4) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `expense_name`
--

INSERT INTO `expense_name` (`id`, `name`, `view`) VALUES
(1, 'teacher', 1),
(2, 'employee', 1),
(3, 'other', 1),
(4, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE IF NOT EXISTS `fee` (
  `fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id_f` varchar(50) NOT NULL,
  `qist_count` tinyint(3) unsigned NOT NULL,
  `amount` mediumint(8) unsigned NOT NULL,
  `fee_date` date NOT NULL,
  `note` text,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`fee_id`),
  KEY `st_id_f` (`st_id_f`),
  KEY `user_id_f` (`user_id_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`fee_id`, `st_id_f`, `qist_count`, `amount`, `fee_date`, `note`, `user_id_f`, `view`) VALUES
(1, 'CS1', 1, 125000, '2015-10-11', '', 1, 1),
(2, 'GO1', 1, 150000, '2015-10-06', 'parai yakam', 1, 1),
(3, 'CS1', 2, 200000, '2015-10-15', NULL, 1, 1),
(4, 'CS1', 3, 125000, '2015-10-20', NULL, 1, 1),
(5, 'GO1', 2, 350000, '2015-10-10', NULL, 1, 1),
(6, 'CS1', 4, 5828258, '2015-10-20', 'جەند تێبینیەک جەند تێبینیەک  جەند تێبینیەک  جەند تێبینیەک  جەند تێبینیەک ', 1, 0),
(7, 'CS3', 0, 500000, '2015-10-07', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `st_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'date of birth',
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `dep` varchar(40) NOT NULL COMMENT 'departmant',
  `stage` varchar(15) NOT NULL,
  `fee` bigint(20) unsigned NOT NULL,
  `discount` int(255) unsigned NOT NULL DEFAULT '0',
  `total_fee` bigint(20) unsigned NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1' COMMENT '1:visible, 0:deleted',
  PRIMARY KEY (`st_id`),
  KEY `user_id_f` (`user_id_f`),
  KEY `user_id_f_2` (`user_id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`st_id`, `fname`, `mname`, `lname`, `dob`, `address`, `phone_no`, `dep`, `stage`, `fee`, `discount`, `total_fee`, `user_id_f`, `view`) VALUES
('CS1', 'ئازاد', 'هێمن', 'جەلیل', '1994-02-27', 'هەولێر', '0750854123', 'Computer Science', '2', 1800000, 800000, 1000000, 1, 1),
('CS3', 'test', 'ioiy', 'test2', '2000-02-02', 'hawler', '5487848458', 'Computer Science', '1', 1000000, 0, 1000000, 1, 0),
('GO1', 'هەژار', 'قادر', 'غەریب', '2015-10-05', 'سلێمانی', '07505471258', 'Gas &amp; Oil', '1', 2500000, 0, 2500000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `role` int(11) NOT NULL,
  `view` tinyint(4) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `username`, `password`, `salt`, `role`, `view`) VALUES
(1, 'admin', '137cc2521180d5468c32d8ba8acfd9b0ca8b7b19', 'JM6@z', 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `exp_name_fore` FOREIGN KEY (`e_id_f`) REFERENCES `expense_name` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `fee`
--
ALTER TABLE `fee`
  ADD CONSTRAINT `relation` FOREIGN KEY (`st_id_f`) REFERENCES `student` (`st_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_foreign_2` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `user_id_foreign` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
