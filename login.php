<?php
if(!isset($_SESSION))
{
    session_start();
}

require_once "./includes/connection.php";
require_once "./includes/functions.php";
if (logged_in()) 
{
    if (isset($_SESSION['admin'])) {
        header("Location: index.php");
        exit;
    }
}
$login_error_message = "";
    // login
    if (isset($_POST['login']) && 
        isset($_POST['username']) && 
        !empty($_POST['username']) &&
        isset($_POST['password']) &&
        !empty($_POST['password'])
        )
    {
        $username = safe(trim($_POST['username']));
        $password = safe(trim($_POST['password']));

        $query = "SELECT * FROM user WHERE username='$username' AND view=1 LIMIT 1";
        $result = mysql_query($query) or die("login".mysql_error());
        
        if(mysql_num_rows($result) == 1)
        {
            $admin = mysql_fetch_array($result);
            $db_pass = $admin['password'];
            $password = sha1(md5($password).$admin['salt']);
            // echo $password;
            // echo "<br>".$admin['password'];
            if($password == $admin['password'])
            {
                // if ($admin['role'] == 1) //if user is admin
                // {
                    $admin_browser = $_SERVER['HTTP_USER_AGENT'];
                    $_SESSION['login_string'] = hash('sha512', $password.$admin_browser);
                    $_SESSION['user_id'] = $admin['u_id'];
                    $_SESSION['name'] = $admin['username'];
                    $_SESSION['fullname'] = $admin['full_name'];
                    $_SESSION['admin'] = $admin['role'];
                    //$_SESSION['error'] = 0;
                    header('Location: index.php');
                    exit;
                // }
            }
            else
            {
                //$_SESSION['error'] = 1;
                $login_error_message .= "Wrong user credentials, please check your user name and/or password.";
            }
            
        }
        else
        {
            $login_error_message .= "Wrong user credentials, please check your user name and/or password.";
        }

    }
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Login</title>

    <link rel="stylesheet" href="css/bootstrap-3.1.1.min.css">
    <style type="text/css">

        body
        {
            background-color: #F8F8F8;
        }

        .login-panel
        {
            margin-top: 7em;
        }

        .errmsj
        {
            margin-top: 2em;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form" method="POST" action="">
                            <fieldset>
                                <div class="form-group <?php echo !empty($login_error_message)? "has-error":""; ?>">
                                    <input class="form-control" required="required" autocomplete="false" placeholder="User Name" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group <?php echo !empty($login_error_message)? "has-error":""; ?>">
                                    <input class="form-control" required="required" autocomplete="false" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <button class="btn btn-lg btn-success btn-block" name="login">Sign In</button>
                            </fieldset>
                            <?php echo !empty($login_error_message)? "
                                <div class=\"form-group errmsj\">
                                    <div>
                                        <div class=\"alert alert-danger\">$login_error_message</div>
                                    </div>
                                </div>":""; 
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
