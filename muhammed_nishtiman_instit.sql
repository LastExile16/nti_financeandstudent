-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2015 at 02:28 PM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `muhammed_nishtiman_instit`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `ex_id` int(10) unsigned NOT NULL,
  `e_id_f` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `user_id_f` varchar(255) DEFAULT NULL COMMENT 'manager',
  `invoice_number` int(10) unsigned DEFAULT NULL,
  `view` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expense_name`
--

CREATE TABLE IF NOT EXISTS `expense_name` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'expense name',
  `order` int(11) unsigned NOT NULL,
  `view` tinyint(4) unsigned DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_name`
--

INSERT INTO `expense_name` (`id`, `name`, `order`, `view`) VALUES
(1, 'teacher', 1, 1),
(2, 'employee', 2, 1),
(3, 'stationary', 3, 1),
(4, 'generator', 4, 1),
(5, 'others', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `in_id` int(11) NOT NULL,
  `invoice_number` bigint(20) unsigned NOT NULL,
  `st_owed_fee_id_f` int(11) NOT NULL,
  `amount` mediumint(8) unsigned NOT NULL,
  `fee_date` date NOT NULL,
  `note` text NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`in_id`, `invoice_number`, `st_owed_fee_id_f`, `amount`, `fee_date`, `note`, `user_id_f`, `view`) VALUES
(7, 1, 7, 1000000, '2015-11-01', 'قستى يةكةم', 1, 1),
(8, 2, 8, 1800000, '2015-11-03', 'Qsty Yakam', 1, 1),
(9, 3, 166, 2000000, '2015-11-23', 'لة جياتى قستى مانكى يةكةم', 1, 1),
(10, 4, 157, 450000, '2015-11-24', '  لةجياتى قستى كرى خويندن', 1, 1),
(11, 5, 247, 700000, '2015-11-24', 'لةجياتى كرى خويندن', 1, 1),
(12, 6, 168, 1000000, '2015-11-25', 'لةجياتى كرى خويندن', 1, 1),
(13, 7, 229, 850000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(14, 8, 282, 600000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(15, 9, 233, 700000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(16, 10, 207, 1000000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(17, 11, 283, 1000000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(18, 12, 98, 1800000, '2015-11-29', 'لةجياتى كرى خويندن', 1, 1),
(19, 13, 26, 900000, '2015-11-24', 'لەجیاتی کرێی خوێندن', 1, 1),
(20, 14, 39, 800000, '2015-11-24', 'لە جیاتی کرێی خوێندن', 1, 1),
(21, 15, 155, 450000, '2015-12-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(22, 16, 58, 450000, '2015-11-15', 'لەجیاتی کرێی خویندن', 1, 1),
(23, 17, 175, 500000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(24, 18, 201, 1000000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(25, 19, 195, 1000000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(26, 20, 243, 625000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(27, 21, 256, 625000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(28, 22, 264, 635000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(29, 23, 237, 625000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(30, 24, 253, 625000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(31, 25, 173, 1600000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(32, 26, 139, 620000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(33, 27, 145, 450000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(34, 28, 153, 600000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(35, 29, 116, 600000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(36, 30, 77, 600000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(37, 31, 143, 1700000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(38, 32, 42, 1700000, '2015-11-05', 'لەجیاتی کرێی خوێندن', 1, 1),
(39, 33, 147, 600000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(40, 34, 79, 600000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(41, 35, 104, 900000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(42, 36, 156, 600000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(43, 37, 31, 600000, '2015-11-01', 'لەجیاتی کرێی خوێندن  ', 1, 1),
(44, 38, 133, 500000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(45, 39, 24, 600000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(46, 40, 20, 600000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(47, 41, 10, 900000, '2015-11-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(48, 42, 59, 600000, '2015-11-15', 'لەجیاتی کرێی خوێندن', 1, 1),
(49, 43, 13, 500000, '2015-11-19', 'لەجیاتی کرێی خوێندن', 1, 1),
(50, 44, 67, 1500000, '2015-11-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(51, 45, 101, 800000, '2015-11-09', 'لەجیاتی کرێی خوێندن', 1, 1),
(52, 46, 82, 850000, '2015-11-09', 'لەجیاتی کرێی خوێندن', 1, 1),
(53, 47, 265, 1250000, '2015-10-29', 'لەجیاتی کرێی خوێندن', 1, 1),
(54, 48, 122, 900000, '2015-10-29', 'لەجیاتی کرێی خوێندن', 1, 1),
(55, 49, 198, 900000, '2015-10-29', 'لەجیاتی کرێی خوێندن', 1, 1),
(56, 50, 209, 700000, '2015-10-29', 'لەجیاتی کرێی خوێندن', 1, 1),
(57, 51, 217, 1900000, '2015-11-09', 'لەجیاتی کرێی خوێندن', 1, 1),
(58, 52, 278, 500000, '2015-11-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(59, 53, 199, 600000, '2015-12-02', 'له جياتى كريَى خويَندن', 1, 1),
(60, 54, 32, 450000, '2015-12-02', 'له جياتى كريَى خويَندن', 1, 1),
(61, 55, 206, 500000, '2015-11-02', 'له جياتى كريَى خويَندن', 1, 1),
(62, 56, 191, 1000000, '2015-11-02', 'لەجیاتی کرێی خوێندن', 1, 1),
(63, 57, 248, 750000, '2015-11-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(64, 58, 214, 1000000, '2015-11-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(65, 59, 200, 1900000, '2015-12-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(66, 60, 273, 750000, '2015-11-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(67, 61, 246, 600000, '2015-12-11', 'لەجیاتی کرێی خوێندن', 1, 1),
(68, 62, 275, 1250000, '2015-11-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(69, 63, 171, 680000, '2015-11-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(70, 64, 183, 1000000, '2015-12-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(71, 65, 218, 1000000, '2015-12-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(72, 66, 225, 1000000, '2015-12-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(73, 67, 181, 1000000, '2015-11-02', 'لەجیاتی کرێی خوێندن', 1, 1),
(74, 68, 185, 1000000, '2015-11-02', 'لەجیاتی کرێی خوێندن', 1, 1),
(75, 69, 272, 630000, '2015-11-02', 'لەجیاتی کرێی خوێندن', 1, 1),
(76, 70, 235, 625000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(77, 71, 277, 750000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(78, 72, 249, 1250000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(79, 73, 202, 665000, '2015-11-10', 'لەجیاتی کرێی خوێندن', 1, 1),
(80, 74, 204, 665000, '2015-11-10', 'لەجیاتی کرێی خوێندن', 1, 1),
(81, 75, 213, 665000, '2015-11-10', 'لەجیاتی کرێی خوێندن', 1, 1),
(82, 76, 69, 500000, '2015-11-10', 'لەجیاتی کرێی خوێندن', 1, 1),
(83, 77, 71, 1700000, '2015-11-30', 'لەجیاتی کرێی خوێندن', 1, 1),
(84, 78, 296, 1500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(85, 79, 297, 500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(86, 80, 298, 1000000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(87, 81, 299, 750000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(88, 82, 12, 500000, '2015-12-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(89, 83, 300, 600000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(90, 84, 301, 500000, '2015-10-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(91, 85, 302, 500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(92, 86, 287, 500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(93, 87, 303, 500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(94, 88, 304, 700000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(95, 89, 64, 400000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(96, 90, 126, 400000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(97, 91, 305, 750000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(98, 92, 148, 400000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(99, 93, 306, 1250000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(100, 94, 307, 500000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(101, 95, 284, 1250000, '2015-12-06', 'لەجیاتی کرێی خوێندن', 1, 1),
(102, 96, 260, 800000, '2015-11-30', 'لەجیاتی کرێی خوێندن', 1, 1),
(103, 97, 261, 525000, '2015-11-08', 'لەجیاتی کرێی خوێندن', 1, 1),
(104, 98, 232, 1250000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(105, 99, 259, 835000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(106, 100, 270, 1250000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(107, 101, 180, 1000000, '2015-11-30', 'لەجیاتی کرێی خوێندن', 1, 1),
(108, 102, 179, 700000, '2015-11-30', 'لەجیاتی کرێی خوێندن', 1, 1),
(109, 103, 186, 500000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(110, 104, 222, 700000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(111, 105, 212, 1000000, '2015-11-04', 'لەجیاتی کرێی خوێندن', 1, 1),
(112, 106, 174, 1000000, '2015-11-01', 'لەجیاتی کرێی خوێندن', 1, 1),
(113, 107, 178, 1000000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(114, 108, 230, 700000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(115, 109, 268, 1250000, '2015-11-03', 'لەجیاتی کرێی خوێندن', 1, 1),
(116, 110, 169, 500000, '2015-11-05', 'لەجیاتی کرێی خوێندن', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `st_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'date of birth',
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `dep` varchar(40) NOT NULL COMMENT 'departmant',
  `stage` varchar(15) NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1' COMMENT '1:visible, 0:deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`st_id`, `fname`, `mname`, `lname`, `dob`, `address`, `phone_no`, `dep`, `stage`, `user_id_f`, `view`) VALUES
('1120001', 'Aso', 'Ali', 'kaka', '2015-10-27', 'rqeb-1999@gmail.com', '07504454545', 'Computer Science', '2', 1, 0),
('113001', 'ahmed', 'Ebrahim', 'Yasen', '2015-10-28', 'sodad@yahoo.com', '07504441212', 'Computer Science', '3', 1, 1),
('113002', 'Ahmed', 'Esmael', 'Rafeq', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113003', 'Ahmed', 'Hatam', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113004', 'Ahmed', 'Khalid', 'Khalaf', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113005', 'Ahmed', 'Khalel', 'Ebrahim', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113006', 'Ahmed', 'Talb', 'Mansur', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113007', 'Ahmed', 'Abdulazez', 'Usman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113008', 'Ahmed', 'Muhmmed', 'Shukr', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113009', 'Ahmed', 'Najm', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113010', 'Arsalan', 'Samal', 'Jabar', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113011', 'Azad', 'Abdulmutaleb', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113012', 'Usama', 'Farhang', 'Zakarya', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113013', 'Esmael', 'Ahmed', 'Hassan', '0000-00-00', 'k', '5', 'Computer Science', '3', 1, 1),
('113014', 'Aso', 'Azad', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113015', 'Andy', 'Janan', 'Jamel', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113016', 'Awos', 'Najm', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113017', 'Elaf', 'Sherzad', 'Kamaran', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113018', 'Ayub', 'Usman', 'Abd', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113019', 'Ayub', 'Qaes', 'Husen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113020', 'Ahmed', 'Marewan', 'Salah', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113021', 'Amena', 'Yarub', 'Edres', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113022', 'Amen', 'Ahmed', 'Sleman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113023', 'Aram', 'Umer', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113024', 'Azad', 'Ali', 'Haeni', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113025', 'Aso', 'Amer', 'Hawar', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113026', 'Avar', 'Luqman', 'Sdeq', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113027', 'Ako', 'Faeruz', 'Mahyden', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113028', 'Rezan', 'Mamand', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113029', 'Raber', 'Ali', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113030', 'Rawand', 'Umer', 'Mahmud', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113031', 'Rebaz', 'Ababakr', 'Azez', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113032', 'Reben', 'Nawzad', 'Msto', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113033', 'Zana', 'Talhat', 'Abdulrazaq', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113034', 'Zrng', 'Sarwat', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113035', 'Sabat', 'Nazhad', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113036', 'Sara', 'Sahd', 'Tahsen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113037', 'Saher', 'Salh', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113038', 'Sarbast', 'Ebrahim', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113039', 'Smko', 'Soran', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113040', 'Sanger', 'Khder', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113041', 'Sausan', 'Usamaden', 'Shamsaden', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113042', 'Suryas', 'Suleman', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113043', 'Sevan', 'Samer', 'Shukr', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113044', 'Sufyan', 'Usman', 'Abd', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113045', 'Shkar', 'Arya', 'Khder', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113046', 'Shna', 'Amer', 'Mahmud', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113047', 'Shekhan', 'Essa', 'Shawlat', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113048', 'Shelan', 'Husen', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113049', 'Safe', 'Gazi', 'Esmael', '0000-00-00', 'K', '4', 'Computer Science', '3', 1, 1),
('113050', 'Shewaw', 'Ahmed', 'Shammlakhan', '0000-00-00', 'K', '4', 'Computer Science', '3', 1, 1),
('113051', 'Salh', 'Safen', 'Ahmed', '0000-00-00', 'K', '4', 'Computer Science', '3', 1, 1),
('113052', 'Gazng', 'Sarwat', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113053', 'Goran', 'Husen', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113054', 'Goran', 'Salh', 'Mustafa', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113055', 'Govan', 'Khalid', 'Abod', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113056', 'Mawlan', 'Anwer', 'Jbrael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113057', 'Nawzad', 'Haeni', 'Yaba', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113058', 'Negar', 'Ahmed', 'Khder', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113059', 'Newar', 'Muhmmed', 'A', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113060', 'Hana', 'Hersh', 'Kakahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113061', 'Hawkar', 'Jange', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113062', 'Harem', 'Abdulqahar', 'Abdulmajed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113063', 'Hawkar', 'Usman', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113064', 'Halwest', 'Azad', 'Suliman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113065', 'Huner', 'Jawad', 'Jalal', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113066', 'Hyam', 'Hashm', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113067', 'Hemdad', 'Abdulrahman', 'Rashed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113068', 'Hemn', 'Hamed', 'Azez', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113069', 'Wazer', 'Khalid', 'Usman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113070', 'Wasem', 'Fahme', 'Gorges', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113071', 'Yusef', 'Hassan', 'Khalel', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113072', 'Yunis', 'Sdeq', 'Hamad', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113073', 'Tarq', 'Zeyad', 'Essa', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113074', 'Humam', 'Musana', 'Kamal', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113075', 'Ahmed', 'Farhad', 'Jalal', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113076', 'Hangaw', 'Abdulsamad', 'A', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113077', 'Raber', 'Namq', 'Majed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113078', 'Dlovan', 'Esmael', 'Sabr', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113079', 'Wlat', 'Jalal', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113080', 'Mahmud', 'Dred', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113081', 'Hazhar', 'Esmael', 'Shekhhamed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113082', 'Dlshad', 'Taha', 'Saedo', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113083', 'Swara', 'Azez', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113084', 'Alan', 'Rzgar', 'Fathulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113085', 'Awara', 'Hamed', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113086', 'Barham', 'Latef', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113087', 'Bahez', 'Bahroz', 'Qarani', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113088', 'Balen', 'Yaba', 'Rasul', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113089', 'Balen', 'Muhmmed', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113090', 'Bayan', 'Gazi', 'Zrar', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113091', 'Bzhar', 'Muhmmed', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113092', 'Baker', 'Khalid', 'Ebrahim', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113093', 'Bnar', 'Rzgar', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113094', 'Bezhan', 'Muhmmed', 'Yasen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113095', 'Taha', 'Haqe', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113096', 'Trefa', 'Azez', 'Majed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113097', 'Jwned', 'Mustafa', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113098', 'Jwan', 'Umed', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113099', 'Cheya', 'Abdulqader', 'Ebrahim', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113100', 'Jeger', 'Abdulla', 'Usman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113101', 'Hassan', 'Najm', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113102', 'Husen', 'Ali', 'Khater', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113103', 'Hamza', 'Sheet', 'Abas', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113104', 'Hayder', 'Hamed', 'Ebrahim', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113105', 'Khalid', 'Haqe', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113106', 'Dalya', 'Dlawer', 'Yusef', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113107', 'Dana', 'Adl', 'Yasen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113108', 'Dana', 'Yasen', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113109', 'Daner', 'Tahran', 'Mustafa', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113110', 'Dawlat', 'Juj', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113111', 'Deyar', 'Abubaker', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113112', 'Abdulla', 'Muhmmed', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113113', 'Abdulrahem', 'Safa', 'Husen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113114', 'Ali', 'Arkan', 'Abdullatef', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113115', 'Umer', 'Chalak', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113116', 'Umer', 'Najat', 'Abdulrahman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113117', 'Usama', 'Khder', 'Abas', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113118', 'Farman', 'Salah', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113119', 'Fahd', 'Emad', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113120', 'Karzan', 'Saed', 'Mala', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113121', 'Karwan', 'Shkur', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113122', 'Kner', 'Jamel', 'Hassan', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113123', 'Lavan', 'Muhmmed', 'Rasul', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113124', 'Lana', 'Hersh', 'Kakahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113125', 'Luae', 'Sarmed', 'Safa', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113126', 'Muhmmed', 'Azad', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113127', 'Muhmmed', 'Rafhed', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113128', 'Muhmmed', 'Sherzad', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113129', 'Muhmmed', 'Sabah', 'Awni', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113130', 'Muhmmed', 'Amr', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113131', 'Muhmmed', 'Abas', 'Barani', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113132', 'Muhmmed', 'Abdulla', 'Khalo', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113133', 'Muhmmed', 'Faxraden', 'Khurshed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113134', 'Muhmmed', 'Fuad', 'Amen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113135', 'Muhmmed', 'Qader', 'Makhu', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113136', 'Muhmmed', 'Qusae', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113137', 'Mahmud', 'Assad', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113138', 'Marwan', 'Kamaran', 'Azez', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113139', 'Muhanad', 'Emad', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113140', 'Ebrahim', 'Jawher', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113141', 'Muhmmed', 'Ahmed', 'Suliman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113142', 'Kawan', 'Kamaran', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113143', 'Ahmed', 'Shwan', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113144', 'Muhmmed', 'Nshat', 'Taher', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113145', 'Huer', 'Ahmed', 'Suliman', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113146', 'Ali', 'Qader', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113147', 'Raber', 'Usman', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113148', 'Ebrahim', 'Farhad', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113149', 'Jasm', 'Muhmmed', 'Aweze', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113150', 'Rayan', 'Burhan', 'Hayder', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113151', 'saman', 'Khurshed', 'Ebrahim', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113152', 'Saneha', 'Muhmmed', 'Saeed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113153', 'Jamal', 'Abdulla', 'Salh', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113154', 'Sawe', 'Jabar', 'Hamad Amen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113155', 'Muhmmed', 'Ebrahim', 'Husen', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113156', 'Musab', 'Aenaden', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113157', 'Ali', 'Nabaz', 'Naseh', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113158', 'Sebar', 'Muhmmed', 'Hassan', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113159', 'Khatab', 'Umer', 'Fazel', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113160', 'Nawfl', 'Nabel', 'Nazm', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113161', 'Azad', 'Baker', 'Rashed', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113162', 'Abdulbaset', 'Muhmmed', 'Khder', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('113163', 'Adnan', 'Hamza', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '3', 1, 1),
('114001', 'Ebrahim', 'Muhmmed', 'Abdulhamed', '2015-11-01', 'kk', '2525', 'Computer Science', '2', 1, 1),
('114002', 'Ahmed', 'Ebrahim', 'Umer', '0000-00-00', 'hhh', '444', 'Computer Science', '2', 1, 1),
('114003', 'Ahmed', 'Anwer', 'Muhmmed', '0000-00-00', 'kkk', '5555', 'Computer Science', '2', 1, 1),
('114004', 'Ahmed', 'Tahsen', 'qader', '0000-00-00', 'kkk', '88', 'Computer Science', '2', 1, 1),
('114005', 'Ahmed', 'Sunder', 'Sardar', '0000-00-00', 'yyy', '777', 'Computer Science', '2', 1, 1),
('114006', 'Ahmed', 'Mustafa', 'Kamel', '0000-00-00', 'bbb', '55', 'Computer Science', '2', 1, 1),
('114007', 'Ahmed', 'Usman', 'Abdulla', '0000-00-00', 'ggg', '555', 'Computer Science', '2', 1, 1),
('114008', 'Amen', 'Nawzad', 'Assad', '0000-00-00', 'lll', '2222', 'Computer Science', '2', 1, 1),
('114009', 'Ansam', 'Amr', 'Fathi', '0000-00-00', 'bbbb', '3333', 'Computer Science', '2', 1, 1),
('114010', 'Aya', 'Hesham', 'Yunis', '0000-00-00', 'nnn', '7777', 'Computer Science', '2', 1, 1),
('114011', 'Barze', 'Sheraz', 'Jamal', '0000-00-00', 'kkk', '555', 'Computer Science', '2', 1, 1),
('114012', 'Brwa', 'Hayder', 'Perdawd', '0000-00-00', 'uuu', '555', 'Computer Science', '2', 1, 1),
('114013', 'Bushra', 'Bashar', 'Gafur', '0000-00-00', 'kkkk', '999', 'Computer Science', '2', 1, 1),
('114014', 'Belal', 'Zuber', 'Hashm', '0000-00-00', 'kkk', '88', 'Computer Science', '2', 1, 1),
('114015', 'Twana', 'Saeb', 'Abdulkarem', '0000-00-00', 'ggg', '555', 'Computer Science', '2', 1, 1),
('114016', 'Hassan', 'Emad', 'Salh', '0000-00-00', 'hhh', '666', 'Computer Science', '2', 1, 1),
('114017', 'Husen', 'Azad', 'Faqe', '0000-00-00', 'ppp', '666', 'Computer Science', '2', 1, 1),
('114018', 'Hekmet', 'Sardar', 'Esmael', '0000-00-00', 'kkk', '66', 'Computer Science', '2', 1, 1),
('114019', 'Khalid', 'Adnan', 'Sabah', '0000-00-00', 'll', '21', 'Computer Science', '2', 1, 1),
('114020', 'Dalya', 'Hassan', 'Jabar', '0000-00-00', 'kk', '44', 'Computer Science', '2', 1, 1),
('114021', 'Dedar', 'Khurshed', 'Khurshed', '0000-00-00', 'mmm', '222', 'Computer Science', '2', 1, 1),
('114022', 'Rawaz', 'Umer', 'Anwer', '0000-00-00', 'kk', '66', 'Computer Science', '2', 1, 1),
('114023', 'Rawa', 'Hoshyar', 'Ebrahim', '0000-00-00', 'k', '5', 'Computer Science', '2', 1, 1),
('114024', 'Redyar', 'Sahdi', 'Merkhan', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114025', 'Zana', 'Raqeb', 'Jabar', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114026', 'Zena', 'Muhmmed', 'Mahdi', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114027', 'Sarwar', 'Jabar', 'Abdulrahman', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114028', 'Shwan', 'Suleman', 'Usman', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114029', 'Chato', 'Kakalla', 'A', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114030', 'Gorges', 'Adwer', 'Gorges', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114031', 'Taef', 'Sabah', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114032', 'Van', 'Akram', 'Hormz', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114033', 'Velar', 'Fars', 'Azez', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114034', 'Abdulla', 'Burhan', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114035', 'Amar', 'Amer', 'Hormz', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114036', 'Umer', 'Fathi', 'Kaka Hama', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114037', 'Umer', 'Muhmmed', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114038', 'Umer', 'Muhmmed', 'Husen', '0000-00-00', 'k', '25', 'Computer Science', '2', 1, 1),
('114039', 'Fadi', 'Sarmed', 'Safa', '0000-00-00', 'k', '5', 'Computer Science', '2', 1, 1),
('114040', 'Fatma', 'Abdulazez', 'Azger', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114041', 'Karzan', 'Sahed', 'Shahban', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114042', 'Karzan', 'Kawa', 'Rahman', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114043', 'Karzan', 'Kawes', 'Hamad', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114044', 'Kamaladen', 'Jamal', 'Fareq', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114045', 'Lazha', 'Awni', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114046', 'Lawen', 'Sherko', 'A', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114047', 'Lozan', 'Waled', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114048', 'Loae', 'Sarmed', 'Safa', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114049', 'Marsel', 'Nawzad', 'Hormz', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114050', 'Muhmmed', 'Jasm', 'Nader', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114051', 'Muhmmed', 'Sarbl', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114052', 'Mustafa', 'Muhmmed', 'Masud', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114053', 'Mawj', 'Adnan', 'Bahnam', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114054', 'Nashwan', 'Dler', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114055', 'Hajer', 'Jamal', 'Ahmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114056', 'Haran', 'Muhmmed', 'Salem', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114057', 'Hazhar', 'Yusef', 'Mawlud', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114058', 'Harem', 'Faruq', 'Mustafa', '2015-11-16', 'k', '4', 'Computer Science', '2', 1, 1),
('114059', 'Harem', 'Magded', 'qader', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114060', 'Halo', 'Sahdi', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114061', 'Hangaw', 'Khalel', 'Baper', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114062', 'Yusef', 'Salm', 'Jarjes', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114063', 'Waled', 'Seham', 'Fareq', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114064', 'Ahmed', 'Mustafa', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114065', 'Marwan', 'Badeh', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114066', 'Mustafa', 'Mahmud', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('114067', 'Hemdad', 'Abdulrahman', 'Rashed', '0000-00-00', 'k', '4', 'Computer Science', '2', 1, 1),
('115001', 'Usama', 'Sardar', 'Hakem', '1980-10-01', 'ronaki', '07512341292', 'Computer Science', '1', 1, 1),
('115002', 'Andam', 'Tahsen', 'Yasen', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115003', 'Peshawa', 'Muhmmed', 'Reza', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115004', 'Dunya', 'Gasan', 'Yusef', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115005', 'Ranjder', 'Abdulazez', 'Mawlud', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115006', 'Zana', 'Assad', 'Assad', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115007', 'Zana', 'Husen', 'Sabr', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115008', 'Zaenab', 'Dlshad', 'Nuri', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115009', 'Sara', 'Rzgar', 'Jabar', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115010', 'Sara', 'Gasan', 'Yusef', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115011', 'Sara', 'Sardar', 'Abdulwahab', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115012', 'saman', 'Abdulla', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115013', 'Sami', 'Qasm', 'Khder', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115014', 'Sarmed', 'Sabah', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115015', 'Suhela', 'Muzafer', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115016', 'Goran', 'Abdulla', 'Azez', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115017', 'Ali', 'Faruq', 'Saheed', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115018', 'Faraedun', 'Qader', 'Brendar', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115019', 'Karzan', 'Umer', 'Hassan', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115020', 'Lawand', 'Sardar', 'Hassan', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115021', 'Lawen', 'Qader', 'smael', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115022', 'Muhmmed', 'Rzgar', 'Jabar', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115023', 'Muhmmed', 'zrar', 'Musher', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115024', 'Muhmmed', 'Maml', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115025', 'Marewan', 'Abdulazez', 'Esmael', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115026', 'Nur', 'Nawzad', 'Nuri', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115027', 'Neyan', 'Khatab', 'Karem', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115028', 'Hawre', 'Jabar', 'Rahman', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115029', 'Hardi', 'Hemn', 'Salahaden', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115030', 'Handren', 'Sabah', 'Akram', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115031', 'Yusef', 'Jalal', 'Mustafa', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115032', 'Are', 'Ebrahim', 'Mahmud', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115033', 'Abdulla', 'Mahmud', 'Nasr', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115034', 'Ebrahim', 'Hesham', 'Faxraden', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115035', 'Ahmed', 'saher', 'Muhmmed', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115036', 'Ala', 'Zaher', 'Hayder', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115037', 'Almuntaser', 'Bellahabd', 'Alrahem', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115038', 'Husen', 'Ahmed', 'wesamaden', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115039', 'Devd', 'Rahed', 'Hana', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115040', 'Ramz', 'Mazn', 'Abd', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115041', 'Ali', 'Nabel', 'Hassan', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115042', 'Fatma', 'Reza', 'Abdulla', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115043', 'Muhmmed', 'kanhan', 'Ali', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115044', 'Madena', 'Bahjat', 'Sharef', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115045', 'Nadra', 'Nawzad', 'Bulahb', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115046', 'Nuran', 'Fahme', 'Umer', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115047', 'wesam', 'Ahmed', 'Abdulhamed', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115048', 'Sara', 'Safa', 'Salah', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('115049', 'Sarab', 'Safa', 'Salah', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('214001', 'Aram', 'Rzgar', 'Hassan', '0000-00-00', 'gdgddddddddddddd', '075044455', 'Gas &amp; Oil', '2', 1, 1),
('214002', 'Ahmed', 'Mhyeden', 'Gareb', '0000-00-00', 'kkkkkkkkkkkkkkkkk', '07504454545', 'Computer Science', '2', 1, 1),
('214003', 'Esmael', 'Fars', 'Karem', '0000-00-00', 'jjjjjjjjjjjjjjjjjjjjjjj', '0750445555', 'Gas &amp; Oil', '2', 1, 1),
('214004', 'Brez', 'Muhmmed', 'Jahfer', '0000-00-00', 'hhhhhhhhhhhhhhhhhhhh', '275044454545', 'Gas &amp; Oil', '2', 1, 1),
('214005', 'Bokan', 'Dawd', 'Muhmmed', '0000-00-00', 'kkkkkkkkkkkkkkkk', '07504415151', 'Gas &amp; Oil', '2', 1, 1),
('214006', 'Bayar', 'Hade', 'Saheed', '0000-00-00', 'hhhhhhhhhhhhh', '07504464545', 'Gas &amp; Oil', '2', 1, 1),
('214007', 'Belal', 'Kamal', 'hama saheed', '0000-00-00', 'uuuuuuuuuuuuuu', '0475045454', 'Gas &amp; Oil', '2', 1, 1),
('214008', 'Dahel', 'khatab', 'Karem', '0000-00-00', 'hhhhhhhhhhhhh', '04755245', 'Gas &amp; Oil', '2', 1, 1),
('214009', 'Zulfeqar', 'Sdeeq', 'khuja', '0000-10-29', 'yyyyyyyyyyyyyyy', '4545454', 'Gas &amp; Oil', '2', 1, 1),
('214010', 'Raste', 'Husen', 'Hamed', '0000-00-00', 'gggggggggggg', '07504858777', 'Gas &amp; Oil', '2', 1, 1),
('214011', 'Reber', 'Rostam', 'Ebrahem', '0000-00-00', 'gggggggggggggg', '454545', 'Gas &amp; Oil', '2', 1, 1),
('214012', 'Rebeen', 'Assed', 'Umer', '0000-00-00', 'kkkkkkkkkkkkkk', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214013', 'Reben', 'Atem', 'Muhmmed', '0000-00-00', 'eeeeeeeeeeeeeeee', '0750485858', 'Gas &amp; Oil', '2', 1, 1),
('214014', 'Reben', 'Taher', 'Azez', '0000-00-00', 'pppppppppppppp', '545454', 'Gas &amp; Oil', '2', 1, 1),
('214015', 'Zardasht', 'Jamal', 'Anwer', '0000-00-00', 'llllllllllllllll', '454545', 'Gas &amp; Oil', '2', 1, 1),
('214016', 'Zakerya', 'Qasm', 'Muhmmed', '0000-00-00', 'kkkkkkkkkkkkkk', '545454', 'Gas &amp; Oil', '2', 1, 1),
('214017', 'Sarwan', 'Jamel', 'Essa', '0000-00-00', 'yyyyyyyyyyyyyy', '54545', 'Gas &amp; Oil', '2', 1, 1),
('214018', 'Cheya', 'Azad', 'Abdulla', '0000-00-00', 'kkkkkkkkkkk', '54545', 'Gas &amp; Oil', '2', 1, 1),
('214019', 'Abdulqader', 'Saman', 'Muzafer', '0000-00-00', 'kkkkk', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214020', 'Abdulqader', 'Muhmmed', 'Ahmed', '0000-00-00', 'mmmmmmmmm', '54545', 'Gas &amp; Oil', '2', 1, 1),
('214021', 'Ali', 'Amer', 'Tahsen', '0000-00-00', 'kkkkk', '4545', 'Gas &amp; Oil', '2', 1, 1),
('214022', 'Muhmmed', 'Kawa', 'Anwer', '0000-00-00', 'kkkkkkkkkkk', '4545', 'Gas &amp; Oil', '2', 1, 1),
('214023', 'Marwa', 'Muhmmed', 'Zeyad', '0000-00-00', 'gggggg', '654565', 'Gas &amp; Oil', '2', 1, 1),
('214024', 'Harman', 'Yassen', 'Abdulla', '0000-00-00', 'hhhh', '44444', 'Gas &amp; Oil', '2', 1, 1),
('214025', 'Yunes', 'Muhmmed', 'Mawlud', '0000-00-00', 'hgh', '54555', 'Gas &amp; Oil', '2', 1, 1),
('214026', 'Esmet', 'Bshar', 'Rasheed', '0000-00-00', 'hghh', '54545', 'Gas &amp; Oil', '2', 1, 1),
('214027', 'Blind', 'Smko', 'Ebrahim', '0000-00-00', 'hhhhhhhh', '48545', 'Gas &amp; Oil', '2', 1, 1),
('214028', 'Hassan', 'Ali', 'Mahmud', '0000-00-00', 'hhgh', '44444', 'Gas &amp; Oil', '2', 1, 1),
('214029', 'Husen', 'Hazm', 'Esmael', '0000-00-00', 'hhhhh', '5555555', 'Gas &amp; Oil', '2', 1, 1),
('214030', 'Daban', 'Abdul wahab', 'Sadq', '0000-00-00', 'kkkkkkkkk', 'hhhhhhhhhhhhh', 'Gas &amp; Oil', '2', 1, 1),
('214031', 'Deyar', 'Najmadeen', 'Sabr', '0000-00-00', 'hhhh', '4545', 'Gas &amp; Oil', '2', 1, 1),
('214032', 'Raste', 'Brendar', 'Mala Ahmed', '0000-00-00', 'bbbbb', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214033', 'Sarkawt', 'Bahjet', 'Azez', '0000-00-00', 'hghhgg', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214034', 'Safa', 'Najat', 'Ahmed', '0000-00-00', 'kkk', '6666', 'Gas &amp; Oil', '2', 1, 1),
('214035', 'Abdul karem', 'Mustafa', 'Abdulla', '0000-00-00', 'kkk', '5555', 'Gas &amp; Oil', '2', 1, 1),
('214036', 'Usman', 'Saher', 'Ali', '0000-00-00', 'LLLLL', '555', 'Gas &amp; Oil', '2', 1, 1),
('214037', 'Muhmmed', 'Esmael', 'Sadq', '0000-00-00', 'kk', '44', 'Gas &amp; Oil', '2', 1, 1),
('214038', 'Muhmmed', 'Rashad', 'Suar', '0000-00-00', 'nnn', '44', 'Gas &amp; Oil', '2', 1, 1),
('214039', 'Muhmmed', 'Ramze', 'Azez', '0000-00-00', 'kkkkk', '4545', 'Gas &amp; Oil', '2', 1, 1),
('214040', 'Muhmmed', 'Saeed', 'Muhmmed', '0000-00-00', 'ggggg', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214041', 'Muhmmed', 'Salem', 'Khder', '0000-00-00', 'hhhhh', '4545', 'Gas &amp; Oil', '2', 1, 1),
('214042', 'Muhmmed', 'Shamal', 'Sdeeq', '0000-00-00', '545', '454', 'Gas &amp; Oil', '2', 1, 1),
('214043', 'Muhmmed', 'Kawa', 'Mahmud', '0000-00-00', 'kkmm', '222225', 'Gas &amp; Oil', '2', 1, 1),
('214044', 'Muhmmed', 'Najdad', 'Khalel', '0000-00-00', 'lllllk', '45454', 'Gas &amp; Oil', '2', 1, 1),
('214045', 'Masud', 'Faruq', 'Hamed Amen', '0000-00-00', 'kmk', '254', 'Gas &amp; Oil', '2', 1, 1),
('214046', 'Mustafa', 'Nawzad', 'Khalel', '0000-00-00', 'mjn', '454545', 'Gas &amp; Oil', '2', 1, 1),
('214047', 'Harem', 'Khalel', 'Rahman', '0000-00-00', 'ggg', '54545', 'Gas &amp; Oil', '2', 1, 1),
('214048', 'Hewr', 'Hemn', 'Salahaden', '0000-00-00', 'mmm', '1212', 'Gas &amp; Oil', '2', 1, 1),
('214049', 'Hemdad', 'Ahmed', 'Tofeq', '0000-00-00', 'kkk', '6666', 'Gas &amp; Oil', '2', 1, 1),
('214050', 'Yusef', 'Muzafer', 'Taha', '0000-00-00', 'hhhh', '5454', 'Gas &amp; Oil', '2', 1, 1),
('215001', 'Ahmed', 'Reyaz', 'Abdulhamed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215002', 'Ahmed', 'Musadaq', 'Jasar', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215003', 'Ahmed', 'Yusef', 'Abdulqader', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215004', 'Esmael', 'Masud', 'Muhmmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215005', 'Ahmed', 'Hamed', 'Usman', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215006', 'Balen', 'Abdulstar', 'Ahmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215007', 'Bader', 'Reyaz', 'Ahmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215008', 'Bzhar', 'Azez', 'Salem', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215009', 'Bryar', 'Anwer', 'Muhmmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215010', 'Bakr', 'Sami', 'Abdulla', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215011', 'Hassan', 'Ali', 'Salem', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215012', 'Hassan', 'Kakarash', 'Muhmmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215013', 'Husen', 'Abdulmanaf', 'Hamza', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215014', 'Dlovan', 'Smko', 'Najat', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215015', 'Razwan', 'Ebrahim', 'Raza', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215016', 'Ronya', 'Bashar', 'Pero', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215017', 'Salar', 'Sabah', 'Abdulla', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215018', 'Sarkawt', 'Hassan', 'Yunes', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215019', 'Salwan', 'Faesal', 'Muhmmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215020', 'Sliman', 'Malal', 'Azez', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215021', 'Sana', 'Sultan', 'Sdeq', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215022', 'Shahraz', 'Sheraz', 'Ahmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215023', 'Shwan', 'Yasen', 'Ali', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215024', 'Sherwan', 'Mahdi', 'Azez', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215025', 'Abdulrahem', 'Surdash', 'Asef', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215026', 'Abdulla', 'Kamaran', 'Salah', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215027', 'Abdulla', 'Zuher', 'Hamed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215028', 'Abdulla', 'Meqdad', 'Shahab', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215029', 'Usman', 'Abdulkarem', 'Mahmud', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215030', 'Usman', 'Umer', 'Usman', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215031', 'Ali', 'Usman', 'Ahmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215032', 'Esa', 'Muhmmed', 'Rashed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215033', 'Fadi', 'Gazi', 'Amen', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215034', 'Farman', 'Ahmed', 'Mustafa', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215035', 'Lemo', 'Muhmmed', 'Sahdun', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215036', 'Muhmmed', 'Ebrahim', 'Rzuqe', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215037', 'Muhmmed', 'Jalel', 'Robeten', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215038', 'Muhmmed', 'Jamel', 'Karem', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215039', 'Muhmmed', 'Dlshad', 'Sabr', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215040', 'Muhmmed', 'Sabah', 'Rahman', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215041', 'Muhmmed', 'Talhat', 'Jabar', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215042', 'Muhmmed', 'Umer', 'Mahmud', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215043', 'Muhmmed', 'Kamaran', 'Jarjes', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215044', 'Muhmmed', 'Kaewan', 'Umer', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215045', 'Muhmmed', 'Maml', 'Ali', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215046', 'Muhmmed', 'Masud', 'Esmael', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215047', 'Muhmmed', 'Naem', 'Hamud', '0000-00-00', 'k', '4', 'Computer Science', '1', 1, 1),
('215048', 'Marwa', 'Tarq', 'Muhmmed', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215049', 'Mustafa', 'Ahmed', 'Sabah', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215050', 'Mustafa', 'Luqman', 'Hassan', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215051', 'Mansur', 'Ramazan', 'Nuri', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215052', 'Hedi', 'Qando', 'Hassan', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215053', 'Hawser', 'Qader', 'Haulla', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215054', 'Halgurd', 'Seamand', 'Esmael', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215055', 'Hawraz', 'Mustafa', 'Hamad Amen', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215056', 'Hedi', 'Faxer', 'Shukri', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215057', 'Waled', 'Ali', 'Sahdaden', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215058', 'Yusef', 'Rushde', 'Husen', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215059', 'Yusef', 'Rashed', 'Mamrash', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215060', 'Yusef', 'zrar', 'Jamel', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215061', 'Are', 'Qader', 'Hama salh', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1),
('215062', 'Vale', 'Habeb', 'Gorges', '0000-00-00', 'k', '4', 'Gas &amp; Oil', '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `st_owed_fee`
--

CREATE TABLE IF NOT EXISTS `st_owed_fee` (
  `st_owed_fee_id` int(11) NOT NULL,
  `st_id_f` varchar(50) NOT NULL,
  `year` year(4) NOT NULL,
  `fee` bigint(20) unsigned NOT NULL,
  `discount` int(255) unsigned NOT NULL DEFAULT '0',
  `total_fee` bigint(20) unsigned NOT NULL,
  `note` varchar(255) NOT NULL,
  `payment_completed` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:not completed, 1:completed'
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `st_owed_fee`
--

INSERT INTO `st_owed_fee` (`st_owed_fee_id`, `st_id_f`, `year`, `fee`, `discount`, `total_fee`, `note`, `payment_completed`) VALUES
(5, '113002', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(6, '113003', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(7, '113163', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(8, '113009', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(9, '113004', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(10, '113005', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(11, '113006', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(12, '113008', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(13, '113010', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(14, '113011', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(15, '113012', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(16, '113013', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(17, '113014', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(18, '113015', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(19, '113016', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(20, '113017', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(21, '113018', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(22, '113019', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(23, '113020', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(24, '113021', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(25, '113022', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(26, '113023', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(27, '113024', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(28, '113025', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(29, '113026', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(30, '113027', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(31, '113028', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(32, '113029', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(33, '113030', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(34, '113031', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(35, '113032', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(36, '113033', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(37, '113034', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(38, '113035', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(39, '113036', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(40, '113037', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(41, '113039', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(42, '113040', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(43, '113041', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(44, '113042', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(45, '113043', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(46, '113044', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(47, '113045', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(48, '113046', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(49, '113047', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(50, '113048', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(51, '113049', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(52, '113050', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(53, '113051', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(54, '113052', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(55, '113053', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(56, '113054', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(57, '113055', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(58, '113056', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(59, '113057', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(60, '113058', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(61, '113059', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(62, '113060', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(63, '113061', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(64, '113062', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(65, '113063', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(66, '113064', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(67, '113065', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(68, '113066', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(69, '113068', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(70, '113067', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(71, '113069', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(72, '113070', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(73, '113071', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(74, '113072', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(75, '113073', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(76, '113074', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(77, '113075', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(78, '113076', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(79, '113077', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(80, '113078', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(81, '113079', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(82, '113080', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(83, '113081', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(84, '113082', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(85, '113083', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(86, '113084', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(87, '113085', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(88, '113086', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(89, '113087', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(90, '113088', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(91, '113089', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(92, '113090', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(93, '113091', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(94, '113092', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(95, '113093', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(96, '113094', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(97, '113095', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(98, '113096', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(99, '113097', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(100, '113097', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(101, '113098', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(102, '113099', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(103, '113100', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(104, '113101', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(105, '113102', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(106, '113103', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(107, '113104', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(108, '113105', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(109, '113106', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(110, '113107', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(111, '113108', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(112, '113109', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(113, '113110', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(114, '113111', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(115, '113112', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(116, '113113', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(117, '113114', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(118, '113115', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(119, '113116', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(120, '113117', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(121, '113118', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(122, '113119', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(123, '113120', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(124, '113121', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(125, '113122', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(126, '113123', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(127, '113124', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(128, '113125', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(129, '113126', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(130, '113127', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(131, '113128', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(132, '113129', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(133, '113130', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(134, '113131', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(135, '113132', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(136, '113133', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(137, '113134', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(138, '113135', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(139, '113136', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(140, '113137', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(141, '113138', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(142, '113139', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(143, '113140', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(144, '113141', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(145, '113142', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(146, '113143', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(147, '113144', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(148, '113145', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(149, '113146', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(150, '113147', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(151, '113148', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(152, '113149', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(153, '113150', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(154, '113151', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(155, '113152', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(156, '113153', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(157, '113154', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(158, '113155', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(159, '113156', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(160, '113157', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(161, '113158', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(162, '113159', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(163, '113160', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(164, '113161', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(165, '113162', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(166, '114001', 2015, 2000000, 0, 2000000, 'Study Fee', 1),
(167, '114002', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(168, '114003', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(169, '114004', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(170, '114005', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(171, '114006', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(172, '114007', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(173, '114008', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(174, '114009', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(175, '114010', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(176, '114011', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(177, '114012', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(178, '114013', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(179, '114014', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(180, '114015', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(181, '114016', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(182, '114017', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(183, '114018', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(184, '114019', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(185, '114020', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(186, '114021', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(187, '114022', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(188, '114023', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(189, '114024', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(190, '114025', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(191, '114026', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(192, '114027', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(193, '114028', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(194, '114029', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(195, '114030', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(196, '114031', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(197, '114032', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(198, '114033', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(199, '114034', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(200, '114035', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(201, '114036', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(202, '114037', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(203, '114038', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(204, '114039', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(205, '114040', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(206, '114041', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(207, '114042', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(208, '114043', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(209, '114044', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(210, '114045', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(211, '114046', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(212, '114047', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(213, '114048', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(214, '114049', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(215, '114050', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(216, '114051', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(217, '114052', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(218, '114053', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(219, '114054', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(220, '114056', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(221, '114057', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(222, '114058', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(223, '114059', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(224, '114060', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(225, '114061', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(226, '114062', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(227, '114063', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(228, '114064', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(229, '114065', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(230, '114066', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(231, '214001', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(232, '214002', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(233, '214003', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(234, '214004', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(235, '214005', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(236, '214006', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(237, '214007', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(238, '214008', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(239, '214009', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(240, '214010', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(241, '214011', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(242, '214012', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(243, '214013', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(244, '214014', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(245, '214015', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(246, '214016', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(247, '214017', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(248, '214018', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(249, '214019', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(250, '214020', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(251, '214021', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(252, '214022', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(253, '214023', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(254, '214024', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(255, '214025', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(256, '214026', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(257, '214027', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(258, '214028', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(259, '214029', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(260, '214030', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(261, '214031', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(262, '214032', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(263, '214033', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(264, '214034', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(265, '214035', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(266, '214036', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(267, '214037', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(268, '214038', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(269, '214039', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(270, '214040', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(271, '214041', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(272, '214042', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(273, '214043', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(274, '214044', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(275, '214045', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(276, '214046', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(277, '214047', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(278, '214048', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(279, '214049', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(280, '214050', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(282, '113001', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(283, '114067', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(284, '215001', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(285, '215002', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(286, '215003', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(287, '215004', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(288, '215005', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(289, '215006', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(290, '215007', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(291, '215008', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(292, '215009', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(293, '215010', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(294, '215011', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(295, '215012', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(296, '215058', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(297, '115011', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(298, '115001', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(299, '115002', 2015, 2000000, 1000000, 1000000, 'Study Fee', 0),
(300, '114055', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(301, '215052', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(302, '115040', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(303, '215056', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(304, '215034', 2015, 2500000, 0, 2500000, 'Study Fee', 0),
(305, '115017', 2015, 2000000, 0, 2000000, 'Study Fee', 0),
(306, '115024', 2015, 2000000, 0, 2000000, 'study fee', 0),
(307, '215047', 2015, 2500000, 0, 2500000, 'study fee', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL,
  `full_name` varchar(40) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `role` int(11) NOT NULL,
  `view` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `full_name`, `username`, `password`, `salt`, `role`, `view`) VALUES
(1, 'Abdulrazaq Hassan Kakahmad', 'admin', '137cc2521180d5468c32d8ba8acfd9b0ca8b7b19', 'JM6@z', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`ex_id`), ADD KEY `e_id_f` (`e_id_f`), ADD KEY `user_id_f` (`user_id_f`);

--
-- Indexes for table `expense_name`
--
ALTER TABLE `expense_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`in_id`), ADD KEY `st_owed_fee_id_f` (`st_owed_fee_id_f`), ADD KEY `user_id_f` (`user_id_f`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`st_id`), ADD KEY `user_id_f` (`user_id_f`);

--
-- Indexes for table `st_owed_fee`
--
ALTER TABLE `st_owed_fee`
  ADD PRIMARY KEY (`st_owed_fee_id`), ADD KEY `st_id_f` (`st_id_f`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `ex_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expense_name`
--
ALTER TABLE `expense_name`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `st_owed_fee`
--
ALTER TABLE `st_owed_fee`
  MODIFY `st_owed_fee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=308;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
ADD CONSTRAINT `exp_name_fore` FOREIGN KEY (`e_id_f`) REFERENCES `expense_name` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`st_owed_fee_id_f`) REFERENCES `st_owed_fee` (`st_owed_fee_id`) ON UPDATE CASCADE,
ADD CONSTRAINT `invoices_ibfk_2` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE;

--
-- Constraints for table `st_owed_fee`
--
ALTER TABLE `st_owed_fee`
ADD CONSTRAINT `st_owed_fee_ibfk_1` FOREIGN KEY (`st_id_f`) REFERENCES `student` (`st_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
