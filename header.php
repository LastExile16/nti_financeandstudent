<!-- A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>پەیمانگەی نیشتیمانی تەکنیکی</title>
<!-- I have added these two -->
<link rel="stylesheet" href="css/bootstrap-3.1.1.min.css">
<link href="css/custom.css" rel='stylesheet' type='text/css' />

<link href="css/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" href="css/lightbox.css">
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<link rel="stylesheet" href="css/datepicker/css/bootstrap-datepicker3.standalone.css">
<!-- jQuery (necessary JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap-3.1.1.min.js"></script>
<script src="css/datepicker/js/bootstrap-datepicker.js"></script>
<!-- Custom Theme files -->

<script src="js/select2.js"></script>
<link rel="stylesheet" type="text/css" href="css/select2.css"/>
<link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css"/>

<!--//theme style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html" />
<meta charset="UTF-8">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


<style type="text/css">
	/** {
		direction: rtl !important;
	}
	.navbar-nav > li{
		float: right;
	}

	.navbar-nav{
		float: right;
	}

	.navbar-toggle {
    	float: left;
	}
	.copywrite, .copywrite .container, .copywrite .container p{
		direction: ltr !important;
	}*/
	/* Portlet */
	/*.portlet, .search-panel, .student-table, .add-panel{
		float: right;
	}*/
	#logo_a{
		padding: 0;
    	border: 1px solid #E7E7E7;
	}
	#logo_img{
		height: 5em;
		width: 5em;
	}
	.student-table{
		font-size:110%;
	}
	.top-header{
		padding: 4px 0;
	}
	.login-info{
	    float: right;
	    color: white;
	    font-size: small;
	    margin-right: 10px;
	}
	.portlet > .portlet-body.green,
	.portlet.green {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	  /*margin-right: 15px;*/
	  /*padding-right: 15px;*/
	  /*padding-left: 15px;*/
	}
	.portlet.box.green > .portlet-title {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	}
	.portlet.box.green {
	    border: 1px solid #73EAC4;
	}
	table th, table td {
	    /*text-align: right;*/
	    vertical-align: middle;
	}

	/*.contact{
		padding-top: 3em; 
		padding-bottom: 0em; 
		margin-bottom: 1em;
	}
	.pagination li{
		float: right;
	}
	form label{
		float: right !important;
		text-align: left !important;
	}
	*/
	.actionbutton{
		float: right;
		margin-right: -10px;
	}
	.pagination>.active>a, .pagination>.active>span, 
	.pagination>.active>a:hover, 
	.pagination>.active>span:hover, 
	.pagination>.active>a:focus, 
	.pagination>.active>span:focus {
		color: #fff;
		background-color: #428bca;
		border-color: #428bca;
		cursor: default;
	}
	.panel-title{
		font-size: 110%;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		color: #000;
	}

	.modal-body-scroll{
		overflow-y: scroll !important;
		max-height: 25em;
	}
	.fee-modal{
		border: solid black 1px;
    	padding: 11px;
	}
	@page { margin: 0; }
</style>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('.datepick').datepicker({
            format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true
        });
	 	$('.delete_student').click(function()
		{
			if (confirm(" دڵنیای لە سرینەوەی ئەم قوتابیە"))
			{
				var id = $(this).parent().parent().attr('id');
				var data = 'st_delete_id=' + id ;
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/delete_student.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						parent.fadeOut('slow', function() {$(this).remove();});
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});

	 	//activate student
		$('.activate_student').click(function()
		{
			if (confirm(" دڵنیای لە ئەکتیڤ کردنی ئەم قوتابیە"))
			{
				var id = $(this).parent().parent().attr('id');
				var data = 'st_activate_id=' + id ;
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/activate_student.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						parent.fadeOut('slow', function() {$(this).remove();});
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});
		

		//delete fee invoice
		$('.delete_fee').click(function()
		{
			if (confirm(" دڵنیای لە سرینەوەی ئەم وەسڵە"))
			{
				var id = $(this).parent().parent().attr('id');
				if ($(this).parent().attr('class')=="expenses") {
					var data = 'expense_delete_id=' + id ;
				}else if($(this).parent().attr('class')=="edit_expenses"){
					var data = 'edit_expense_delete_id=' + id ;
				}else{
					var data = 'fee_delete_id=' + id ;
				}
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/delete_fee.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						parent.fadeOut('slow', function() {$(this).remove();});
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});

		//change fee type when student name changed in pay fee
		$("#name_code").change(function()
		{
			var code = $(this).val();
			// $("#fee_type").css({"display":"block"});
			// $("#allspan").css({"display":"none"});
			var data = 'code=' + code ;
			//$(#fee_type)
			// alert(code);
			$.ajax(
			{
				   type: "POST",
				   url: "./includes/change_list.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						$("#fee_type").html(data1);
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found'+id);
						}
				   }
			});
		});

		//change owed fee and payed fee when student fee type in pay fee
		$("#fee_type").change(function()
		{
			var st_owed_fee_id = $(this).val();
			var data = 'st_owed_fee_id=' + st_owed_fee_id ;

			$(".loaded").hide();
			$("#loading").show();
			$.ajax(
			{
				type: "POST",
			    url: "./includes/owed_fee_amounts.php",
			    dataType: 'json',
			    data: data,
			    //async: false,
			    cache: false,

			    success: function(data)
			    {
			    	var owed = parseInt(data.owed_fee,10);
			    	var payed = parseInt(data.payed_fee,10)>-1?parseInt(data.payed_fee,10):0;
			        $("#owed_fee").val(data.owed_fee);
			        $("#payed_fee").val(parseInt(data.payed_fee,10)>-1?data.payed_fee:0);
			        $("#remaining_fee").val(owed - payed);
			        $("#loading").hide();
			        $(".loaded").show();
			    }
			});
		});
	});
</script>


</head>
<body>
<!-- header -->
<div class="top-header">
		 <div class="login-info"><span>Logged In as <?php echo $_SESSION['fullname']; ?></span> | <span><a href="logout.php">Logout</a></span></div>
	 <div class="container">
		 <div class="logo">
			<h1>
				<a id="logo_a" href="index.php">
				 	<img id="logo_img" src="./images/logo.jpg">
				</a>
			</h1>
		 </div>	
	 </div>
</div>
<!---->
<div class="top-menu">
	 <div class="container">
	  <div class="content white">
		 <nav class="navbar navbar-default">
			 <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>				
			 </div>
			 <!--/.navbar-header-->		
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				 <ul class="nav navbar-nav">
				 	<?php 
				 		$page="";
				 		if (isset($_GET['page']) && !empty($_GET['page'])) {
				 			$page = $_GET['page'];
				 		}
				 		else{
				 			if ($_SESSION['admin']==1 || $_SESSION['admin']==3 || $_SESSION['admin']==4) {
				 				$page = "homepage";
				 			}elseif ($_SESSION['admin'] == 2) {
				 				$page = "spend_expense";
				 			}
				 		}

				 	 ?>
				 	 <?php  
				 	 	// if full or accountant or viewer admin
				 	 	if ($_SESSION['admin'] == 1 || $_SESSION['admin'] == 3 || $_SESSION['admin'] == 4) {
				 	 ?>
					 <li <?php echo $page == "homepage" || $page == "add_student" || $page == "deactivated_st" ?'class="active"':""; ?> ><a href="index.php">Students</a></li>
				 	 <?php	
				 		}
				 	 ?>
				 	 <?php
				 	 	//if full admin or accountant
				 	 	if ($_SESSION['admin'] == 1 || $_SESSION['admin'] == 3) {
				 	 		
				 	 ?>
					 <li <?php echo $page == "pay_fee" ?'class="active"':""; ?> ><a href="index.php?page=pay_fee">Pay Fee</a></li>
					 <li <?php echo $page == "reports" || $page == "print_invoice"?'class="active"':""; ?> ><a href="index.php?page=reports">Reports</a></li>
				 	 <?php
				 	 	}//end of accountant and viewer IF
				 	 	// if spend of expense admin
				 	 	if ($_SESSION['admin'] == 1 || $_SESSION['admin'] == 2 || $_SESSION['admin'] == 3) {
				 	 		

				 	 ?>
					 <li <?php echo $page == "spend_expense"?'class="active"':""; ?> ><a href="index.php?page=spend_expense">Spend Expense</a></li>
					 <li <?php echo $page == "expenses" || $page == "edit_expenses"?'class="active"':""; ?> ><a href="index.php?page=expenses">Expenses</a></li>
					<?php
				 	 	}//end of spend of expense
				 	 	// if full admin
				 	 	if ($_SESSION['admin']==1) {
				 	 		
					?>
					 <li <?php echo $page == "user_control"?'class="active"':""; ?> ><a href="index.php?page=user_control">User Control</a></li>
					
					<?php } //end of full admin ?>
						<!-- <li class="dropdown">
						<a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Destinations<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="index.html">Destination 1</a></li>
							<li><a href="index.html">Destination 2</a></li>
							<li><a href="index.html">Destination 3</a></li>
							<li><a href="index.html">Destination 4</a></li>
						</ul>
					 </li>	 -->				
				 </ul>
				</div>
			  <!--/.navbar collapse-->
		 </nav>
		  <!--/.navbar-->		 
	  </div>
	 <div class="clearfix"></div>
		<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
		<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
		</div>
</div>