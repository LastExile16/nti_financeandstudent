SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `student` (
  `st_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'date of birth',
  `address` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `dep` varchar(40) NOT NULL COMMENT 'departmant',
  `stage` varchar(15) NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1' COMMENT '1:visible, 0:deleted',
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `st_owed_fee` (
	`st_owed_fee_id` int(11) NOT NULL AUTO_INCREMENT,
	`st_id_f` varchar(50) NOT NULL,
	`year` year NOT NULL,
	`fee` bigint(20) unsigned NOT NULL,
	`discount` int(255) unsigned NOT NULL DEFAULT '0',
	`total_fee` bigint(20) unsigned NOT NULL,
	`note` varchar(255) NOT NULL,
	PRIMARY KEY (`st_owed_fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `invoices` (
  `in_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` bigint(20) unsigned NOT NULL,
  `st_owed_fee_id_f` int(11) NOT NULL,
  `amount` mediumint(8) unsigned NOT NULL,
  `fee_date` date NOT NULL,
  `note` text NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`in_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `role` int(11) NOT NULL,
  `view` tinyint(4) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `user` (`u_id`, `username`, `password`, `salt`, `role`, `view`) VALUES
(1, 'admin', '137cc2521180d5468c32d8ba8acfd9b0ca8b7b19', 'JM6@z', 1, 1);

